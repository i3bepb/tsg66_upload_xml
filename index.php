<?php
// чтобы обрабатывать сигналы из вне
declare(ticks = 1);

// trait появились в php 5.4
trait Singleton
{
    static protected $_instance = null;

    static public function getInstance($param = false)
    {
        if (null === static::$_instance) {
            $className = get_called_class();
            if ($param) {
                static::$_instance = new $className($param);
            } else {
                static::$_instance = new $className();
            }
        }
        return static::$_instance;
    }

    final private function __clone()
    {
    }

}

final class DB extends PDO
{
    use Singleton;
    static protected $_instance = null;

    public function __construct()
    {
        global $app;
        $config = $app->db;
        parent::__construct($config['connectionString'], $config['username'], $config['password'], [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES "' . $config['charset'] . '"'
        ]);
    }

    static public function reConnect()
    {
        self::$_instance = null;
        return self::getInstance();
    }
}

class App
{
    use Singleton;
    static protected $_instance = null;
    /** @var boolean $_parentProc флаг указывающий на то, что данный процесс является хост-процессом */
    public $_parentProc;
    /**
     * @var array $_workerPids массив PID-ов рабочих процессов
     */
    public $_runMainLoop = true;
    public $_workerPids = [];
    public $workerCount = 100;
    public $pidPath = '/var/run/tsg66.pid';
    public $basePath = '/var/webserver/tsg66.ru';
    public $db = [
        'connectionString' => 'pgsql:host=localhost;port=5433;dbname=test',
        'username' => 'username',
        'password' => 'password',
        'charset' => 'utf8',
    ];
    public $api2gisKey = '';

    public function __construct($config = null)
    {
        $this->configure($config);
        $this->_daemonize();
        chdir($this->basePath . '/uploads/');
    }

    public function kill($signal)
    {
        if (!$this->_parentProc) return;
        $this->_runMainLoop = false;
        // убить всех потомков
        $i = 0;
        $c = '';
        foreach($this->_workerPids as $k => $v) {
            if (!$i) ++$i;
            else $c .= ' && ';
            $c .= 'kill -9 ' . $v;
        }
        system($c, $r);
        // а потом самому убится
        if ($r == 0) {
            $this->_message('App kill!');
            if (is_readable($this->pidPath)) {
                $pid = file_get_contents($this->pidPath);
                unlink($this->pidPath);
                exec('kill -9 ' . $pid . ' &');
            }
            exit(1);
        }
    }
    public function plusProc($signal)
    {
        if (!$this->_parentProc) return;
        $step = 10;
        $this->_message('Worker processes count:' . sizeof($this->_workerPids));
        // добавим потоков
        $c = $this->workerCount;
        $c += $step;
        if ($c > 50) $c = 50;
        $this->workerCount = $c;
        $this->_message('Add ' . $step . ' worker processes and now max count:' . $this->workerCount);
    }
    public function minusProc($signal)
    {
        if (!$this->_parentProc) return;
        $step = 10;
        $this->_message('Worker processes count:' . sizeof($this->_workerPids));
        // добавим потоков
        $c = $this->workerCount;
        $c -= $step;
        if ($c < 10) $c = 10;
        $this->workerCount = $c;
        $this->_message('Minus ' . $step . ' worker processes and now max count:' . $this->workerCount);
    }

    /**
     * Логирование
     * @param string $t сообщение
     */
    private function _log($t)
    {
        static $fl;
        if (empty($fl)) {
            $fl = $this->basePath . '/logs/tsg66_' . date('H_i_d_m_Y') . '.log';
        }
        $fp = fopen($fl, 'a-');
        fputs($fp, date('H:i:s d.m.Y') . "\t" . $t . "\n");
        fclose($fp);
    }

    /**
     * Написать что-нибудь в вывод
     * @param string $t сообщение
     * @param bool $log логировать ли?
     */
    private function _message($t, $log = true)
    {
        echo $t . "\n";
        if ($log) {
            $this->_log($t);
        }
    }

    /**
     * Создает хост-процесс, завершает текущий консольный процесс, пишет PID файл.
     * Отцепляемся от консоли
     */
    private function _daemonize()
    {
        $this->_checkPid();
//        // форкает process и первоначальный процесс завершает
//        if (pcntl_fork()) {
//            exit(0);
//        }
//        // делает текущий процесс лидером сессии или убивает его
//        if (posix_setsid() == -1) {
//            $this->_die(-1);
//        }
        if (is_writable(dirname($this->pidPath))) {
            file_put_contents($this->pidPath, posix_getpid());
        } else {
            $this->_message("Unable to save PID to {$this->pidPath}!");
        }
        // помечаем, что это родительский процесс
        $this->_parentProc = true;
        // устанавливаем функцию обработчик сигналов
        pcntl_signal(SIGTERM, array($this, 'kill'));
        pcntl_signal(SIGUSR1, array($this, 'minusProc'));
        pcntl_signal(SIGUSR2, array($this, 'plusProc'));
        $this->_log('tsg66 run');
    }

    /**
     * Проверяет наличие PID файла данного процесса. Если файл есть, но демон не работает, удаляет PID.
     * @return void
     */
    private function _checkPid()
    {
        if (is_readable($this->pidPath)) {
            $pid = (int)file_get_contents($this->pidPath);
            if ($pid > 0 && posix_kill($pid, SIG_DFL)) {
                $this->_message('"tsg66" already running', false);
                exit(0);
            }
            if (!unlink($this->pidPath)) {
                $this->_message('Cannot delete daemon PID-file', false);
                exit(0);
            }
        }
    }

    /**
     * Сохраняет переданные настройки приложения
     * @param array|string $config
     */
    public function configure($config)
    {
        if ($config === null) {
            $this->_message('Not set $config!', false);
            exit(1);
        }
        if (is_string($config)) {
            $config = require($config);
        }
        if (isset($config['basePath'])) {
            $this->setBasePath($config['basePath']);
            unset($config['basePath']);
        } else {
            $this->setBasePath(__DIR__);
        }
        if (is_array($config)) {
            foreach ($config as $k => $v) {
                $this->$k = $v;
            }
        }
    }

    /**
     * Устанавливает основную директорию
     * @param string $path
     */
    private function setBasePath($path)
    {
        if (($this->basePath = realpath($path)) === false || !is_dir($this->basePath)) {
            $this->_message('App base path is not a valid directory!', false);
            exit(1);
        }
    }

    /**
     * Преобразует дату в часть ссылки для скачивания
     * @param string $date 22.01.2015
     * @return string 20150122
     */
    static public function partLink($date)
    {
        $a = explode('.', $date);
        return $a[2] . $a[1] . $a[0];
    }

    static public function dataFromLink($str)
    {
        return substr($str, 6, 2) . '.' . substr($str, 4, 2) . '.' . substr($str, 0, 4);
    }

    /**
     * Функция распаковывания
     */
    private function _unrar()
    {
        /** @var $db PDO */
        $db = DB::getInstance();
        $stm = $db->query('SELECT type_name, val FROM a_settings WHERE type_name IN ("unrar", "db_date") LIMIT 2');
        $f = function ($upd = true, $date = 0) use ($db) {
            // убираем на всякий случай, прежде чем распаковывать
            $c = 'rm -f ' . $this->basePath . '/uploads/AS*.XML';
            $this->_log($c);
            system($c, $r);
            if ($r == 0) {
                // распаковываем
                $c = 'unrar x ' . $this->basePath . '/uploads/fias_xml.rar';
                $this->_message($c);
                system($c, $r);
                if ($r == 0) {
                    $this->_message('Unrar successful!');
                    if ($upd) {
                        $q = 'UPDATE a_settings SET val = ' . $db->quote($date) . ' WHERE type_name = "unrar" LIMIT 1';
                    } else {
                        $q = 'INSERT INTO a_settings (type_name, val) VALUE ("unrar", ' . $db->quote($date) . ')';
                    }
                    $db->exec($q);
                    return true;
                } else {
                    $this->_message('Unrar error!');
                    return false;
                }
            }
        };
        if ($stm->rowCount() < 2) {
            $row = $stm->fetch(PDO::FETCH_ASSOC);
            // если предыдущий шаг не выполнен, этот не стоит делать
            if ($row['type_name'] == 'unrar') return false;
            return $f(false, $row['val']);
        } else {
            $arr = [];
            while ($row = $stm->fetch(PDO::FETCH_ASSOC)) $arr[$row['type_name']] = $row;
            if ($arr['db_date']['val'] == $arr['unrar']['val']) {
                $this->_message('Unrar already!');
                return true;
            } else {
                return $f(true, $arr['db_date']['val']);
            }
        }
    }

    /**
     * Функция предварительной обработки файла xml
     */
    private function _pre()
    {
        /** @var $db PDO */
        $db = DB::getInstance();
        $stm = $db->query('SELECT type_name, val FROM a_settings WHERE type_name IN ("pre", "unrar")');
        $f2 = function ($upd = true, $date = 0) {
            // прежде опять же на всякий случай подтираем все
            $c = 'rm -fr ' . $this->basePath . '/uploads/pre/ && mkdir ' . $this->basePath . '/uploads/pre';
            $this->_log($c);
            system($c, $r);
            // читаем каталог и делим все файлы на части, большие поделяться, маленькие без изменения останутся
            if ($r == 0) {
                // переходим в нужную папку
                chdir($this->basePath . '/uploads/pre/');
                if ($hd = opendir($this->basePath . '/uploads/')) {
                    while (false !== ($entry = readdir($hd))) {
                        if ($entry == '.' || $entry == '..') {
                            continue;
                        }
                        if (false !== strpos($entry, '.XML')) {
                            if ($this->_runMainLoop) {
                                $workerPid = pcntl_fork();
                                if ($workerPid) {
                                    $this->_workerPids[$workerPid] = 1;
                                } else {
                                    $this->_parentProc = false;
                                    // делим на части по 100M
                                    $c = 'split -b 100M ' . $this->basePath . '/uploads/' . $entry . ' ' . substr($entry, 0, -4) . '_';
                                    $this->_message($c);
                                    system($c);
                                    exit(0);
                                }
                            }
                        }
                    }
                    closedir($hd);
                    $this->_waitChild();
                    $this->_message('Pre successful!');
                    $db = DB::reConnect();
                    if ($upd) {
                        $q = 'UPDATE a_settings SET val = ' . $db->quote($date) . ' WHERE type_name = "pre" LIMIT 1';
                    } else {
                        $q = 'INSERT INTO a_settings (type_name, val) VALUE ("pre", ' . $db->quote($date) . ')';
                    }
                    $db->exec($q);
                    return true;
                } else {
                    $this->_message('Opendir error!');
                    return false;
                }
            } else {
                $this->_message('rm pre error!');
                return false;
            }
        };
        if ($stm->rowCount() < 2) {
            $row = $stm->fetch(PDO::FETCH_ASSOC);
            // если предыдущий шаг не выполнен, этот не стоит делать
            if ($row['type_name'] == 'pre') return false;
            return $f2(false, $row['val']);
        } else {
            $arr = [];
            while ($row = $stm->fetch(PDO::FETCH_ASSOC)) $arr[$row['type_name']] = $row;
            if ($arr['pre']['val'] == $arr['unrar']['val']) {
                $this->_message('Pre already!');
                return true;
            } else {
                return $f2(true, $arr['unrar']['val']);
            }
        }
    }
    /**
     * Функция парсинга файла xml
     */
    public function run()
    {
        $this->migration1();
        if (!$this->_upload() || !$this->_unrar() || !$this->_pre() || !$this->_formate()) {
            $this->_die(1);
        }
        if ($this->_parse()) {
            $this->_type();
            $region = '45';
            if ($this->_transfer($region)) {
                $this->_2gis($region);
                $this->_migration3($region);
            }
        }
        $this->_die(0);
    }
    private function _rubrics()
    {
        $rubrics = [
            'Аварийные службы',
            'Справочно-информационные услуги',
            'Эвакуация автомобилей',
            'Управление ГО и ЧС',
            'Пожарная охрана',
            'Скорая медицинская помощь',
            'Независимые службы аварийных комиссаров',
            'Штрафстоянки',
            'Службы спасения',
            'Телефоны доверия',
            'Автозапчасти для иномарок',
            'Авторемонт и техобслуживание (СТО)',
            'Шины / диски',
            'Автомойки',
            'Шиномонтаж',
            'Автозапчасти для отечественных автомобилей',
            'Услуги авторазбора',
            'Автохимия / масла',
            'Автосигнализации - продажа / установка',
            'Автозаправочные станции (АЗС)',
            'Автоаксессуары',
            'Автомобильные аккумуляторы',
            'Технический осмотр транспорта',
            'Ремонт автоэлектрики',
            'Газовое оборудование для автотранспорта',
            'Кузовной ремонт',
            'Автозапчасти для грузовых автомобилей',
            'Специализированное автооборудование',
            'Автозвук',
            'Развал / Схождение',
            'Автостоянки',
            'Ремонт ходовой части автомобиля',
            'Ремонт карбюраторов / инжекторов',
            'Ремонт электронных систем управления автомобиля',
            'Установка / ремонт автостёкол',
            'Ремонт бензиновых двигателей',
            'Автостекло',
            'Тюнинг',
            'Аппаратная замена масла',
            'Запчасти к сельхозтехнике',
            'Тонирование автостёкол',
            'Автоэмали',
            'Выездная техническая помощь на дороге',
            'Ремонт',
        ];
        $page = $this->_send('http://salebases.ru/stati/rubriki_i_podrubriki_spravochnika_dubl_gis.html');
        $page = mb_convert_encoding($page, 'utf-8', 'cp1251');
        $pos1 = strpos($page, 'Список подрубрик:');
        if ($pos1 !== false) {
            $pos2 = strpos($page, 'end #posts');
            $str = substr($page, $pos1, $pos2);
            preg_match_all('#<td align="LEFT">(.+?)</td>#s', $str, $match);
            system('touch ' . $this->basePath . '/rubrics');
            foreach($match[1] as $v) {
                if (in_array($v, $rubrics)) continue;
                system('echo "\'' . $v . '\'," >> ' . $this->basePath . '/rubrics');
            }
        }
    }

    /**
     * Функция парсинга файла xml
     */
    public function test()
    {
        /** @var PDO $db */
        $db = DB::getInstance();
        $stmn = $db->query('SELECT house_id, `name`, count(*) FROM house_organizations GROUP BY house_id, `name` HAVING count(*) > 1');
        if ($stmn->rowCount() > 0) {
            $q = '';
            while($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
                $q .= 'DELETE FROM house_organizations WHERE `name` = ' . $db->quote($row['name']) . ' AND house_id = ' . $row['house_id'] . ' LIMIT 1;';
            }
            $db->exec($q);
        }
    }

    /**
     * Поиск ссылки на html странице
     * @return array ссылка на файл с БД сжатой в rar
     */
    private function _findLinkHtml()
    {
        $page = file_get_contents('http://fias.nalog.ru/Public/DownloadPage.aspx', false, stream_context_create([
            'http' => ['header' => "User-agent:Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0\nConnection:close"]
        ]));
        if (!preg_match_all('#БД ФИАС от (\d{2}\.\d{2}\.\d{4})#iu', $page, $match)) {
            $this->_message('Not found link from page!');
            $this->_die(1);
        }
        return [
            $match[1][1],
            'http://fias.nalog.ru/Public/Downloads/' . self::partLink($match[1][1]) . '/fias_xml.rar'
        ];
    }

    /**
     * Получение ссылки через soap
     * @return array ссылка на файл с БД сжатой в rar
     */
    private function _findLinkSoap()
    {
        $urlOrig = 'http://fias.nalog.ru/WebServices/Public/DownloadService.asmx';
        $content = '<?xml version="1.0" encoding="utf-8"?><soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope"><soap12:Body><GetLastDownloadFileInfo xmlns="http://fias.nalog.ru/WebServices/Public/DownloadService.asmx" /></soap12:Body></soap12:Envelope>';
        $url = parse_url($urlOrig);
        $arr = [
            $url['scheme'] => [
                'method' => 'POST',
                'content' => $content,
                'header' =>
                    'Host: ' . $url['host'] . "\n" .
                    "Content-Type: application/soap+xml; charset=utf-8\n" .
                    'Content-length: ' . strlen($content) . "\n"
            ]
        ];
        $r = file_get_contents($urlOrig, false, stream_context_create($arr));
        if (!preg_match('#FiasCompleteXmlUrl>(.+?/(\d+?)/.+?)</FiasCompleteXmlUrl#is', $r, $match)) {
            $this->_message('Link not found!');
            $this->_die(1);
        }
        return [self::dataFromLink($match[2]), $match[1]];
    }

    /**
     * Поиск на странице ссылку на архив c БД в xml и скачивание
     * @param bool $soap найти ссылку на БД через html или soap
     * @return bool скачан ли файл с БД
     */
    private function _upload($soap = true)
    {
        if ($soap) list($d, $url) = $this->_findLinkSoap();
        else list($d, $url) = $this->_findLinkHtml();
        /** @var $db PDO */
        $db = DB::getInstance();
        // функция закачивания fias_xml.rar
        $f = function ($upd, $url) use ($d, $db) {
            $c = 'rm -f ' . $this->basePath . '/uploads/fias_xml.rar';
            $this->_log($c);
            system($c, $r);
            if ($r == 0) {
                $c = 'wget -P ' . $this->basePath . '/uploads/ ' . $url;
                $this->_log($c);
                system($c, $r);
                if ($r != 0) {
                    // не скачалось, надо разобраться в чем причина и повторить скачивание
                    $this->_message("File 'fias_xml.rar' upload error!");
                    return false;
                }
                if ($upd) $q = 'UPDATE a_settings SET val = ' . $db->quote($d) . ' WHERE type_name = "db_date"';
                else $q = 'INSERT INTO a_settings (type_name, val) VALUE ("db_date", ' . $db->quote($d) . ')';
                $db->exec($q);
                $this->_message("File 'fias_xml.rar' upload successful!");
            }
            return true;
        };
        $stmn = $db->query('SELECT val FROM a_settings WHERE type_name = "db_date" LIMIT 1');
        if ($stmn->rowCount() > 0) {
            $lastDb = $stmn->fetchColumn();
            if (strtotime($lastDb) < strtotime($d)) {
                return $f(true, $url);
            } else {
                $this->_message("File 'fias_xml.rar' already uploaded!");
                return true;
            }
        } else {
            return $f(false, $url);
        }
    }

    public function migration1()
    {
        $db = DB::getInstance();
        $db->exec('
            CREATE TABLE IF NOT EXISTS `a_settings` (
                `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
                `type_name` enum("db_date", "pre", "parse", "unrar", "formate", "transfer") NOT NULL default "db_date",
                `val` varchar(100) NOT NULL DEFAULT "",
                PRIMARY KEY (`id`),
                KEY `k_a_settings1` (`type_name`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
            CREATE TABLE IF NOT EXISTS `a_object` (
                `aoid` char(36) NOT NULL,
                `aoguid` char(36) NOT NULL DEFAULT "",
                `parentguid` char(36) DEFAULT NULL,
                `formalname` varchar(100) DEFAULT NULL,
                `offname` varchar(120) DEFAULT NULL,
                `shortname` varchar(10) DEFAULT NULL,
                `aolevel` tinyint(3) DEFAULT NULL,
                `regioncode` tinyint(2) DEFAULT NULL,
                `areacode` varchar(5) DEFAULT NULL,
                `autocode` varchar(5) DEFAULT NULL,
                `citycode` varchar(5) DEFAULT NULL,
                `ctarcode` varchar(5) DEFAULT NULL,
                `placecode` varchar(5) DEFAULT NULL,
                `streetcode` varchar(5) DEFAULT NULL,
                `extrcode` varchar(5) DEFAULT NULL,
                `sextcode` varchar(5) DEFAULT NULL,
                `plaincode` varchar(15) DEFAULT NULL,
                `code` varchar(17) DEFAULT NULL,
                `currstatus` varchar(3) DEFAULT NULL,
                `actstatus` tinyint(1) DEFAULT NULL,
                `livestatus` tinyint(2) DEFAULT NULL,
                `centstatus` tinyint(2) DEFAULT NULL,
                `operstatus` tinyint(2) DEFAULT NULL,
                `ifnsfl` varchar(4) DEFAULT NULL,
                `ifnsul` varchar(4) DEFAULT NULL,
                `terrifnsfl` varchar(4) DEFAULT NULL,
                `terrifnsul` varchar(4) DEFAULT NULL,
                `okato` varchar(11) DEFAULT NULL,
                `oktmo` varchar(11) DEFAULT NULL,
                `postalcode` varchar(6) DEFAULT NULL,
                `startdate` date DEFAULT NULL,
                `enddate` date DEFAULT NULL,
                `updatedate` date DEFAULT NULL,
                `previd` char(36) DEFAULT NULL,
                `nextid` char(36) DEFAULT NULL,
                `normdoc` char(36) DEFAULT NULL,
                PRIMARY KEY (`aoid`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
            CREATE TABLE IF NOT EXISTS `a_house` (
                `houseid` char(36) NOT NULL,
                `houseguid` char(36) DEFAULT NULL,
                `aoguid` char(36) DEFAULT NULL,
                `housenum` varchar(10) DEFAULT NULL,
                `strstatus` varchar(10) DEFAULT NULL,
                `eststatus` tinyint(1) DEFAULT NULL,
                `statstatus` tinyint(1) DEFAULT NULL,
                `ifnsfl` varchar(4) DEFAULT NULL,
                `ifnsul` varchar(4) DEFAULT NULL,
                `terrifnsfl` varchar(4) DEFAULT NULL,
                `terrifnsul` varchar(4) DEFAULT NULL,
                `okato` varchar(11) DEFAULT NULL,
                `oktmo` varchar(11) DEFAULT NULL,
                `postalcode` varchar(6) DEFAULT NULL,
                `startdate` date DEFAULT NULL,
                `enddate` date DEFAULT NULL,
                `updatedate` date DEFAULT NULL,
                `counter` varchar(15) DEFAULT NULL,
                `normdoc` char(36) DEFAULT NULL,
                `buildnum` varchar(10) DEFAULT NULL,
                `strucnum` varchar(10) DEFAULT NULL,
                PRIMARY KEY (`houseid`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
            DROP TABLE IF EXISTS `a_settlement_type`;
            CREATE TABLE `a_settlement_type` (
                `shortname` varchar(15) NOT NULL DEFAULT "",
                `fullname` varchar(100) NOT NULL DEFAULT "",
                PRIMARY KEY (`shortname`),
                unique KEY `uniq_a_reduction1` (`shortname`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
            -- эту таблицу заполняем в ручную из-за того что нужно точное соответствие названии, например в fias "город" с маленькой большой буквы
            INSERT INTO `a_settlement_type` (`fullname`, `shortname`) VALUES ("город", "г"), ("город", "г."), ("поселок городского типа", "пгт."),
                ("поселок городского типа", "пгт"), ("поселок", "п"), ("поселок", "п."), ("деревня", "д."), ("деревня", "д"), ("село", "с."),
                ("село", "с"), ("рабочий поселок", "рп."), ("рабочий поселок", "рп"), ("хутор", "х"), ("аул", "аул"), ("станица", "ст-ца"),
                ("станция", "ст"), ("садовое товарищество", "сдт"), ("сельсовет", "с/с"), ("Переулок", "пер"), ("Улица", "ул"), ("Микрорайон", "мкр"),
                ("Территория", "тер"), ("квартал", "кв-л"), ("Проезд", "проезд"), ("линия", "линия"), ("населенный пункт", "нп"), ("местечко", "м"),
                ("Разъезд", "рзд"), ("Шоссе", "ш"), ("район", "р-н"), ("садовое некоммерческое товарищество", "снт"), ("сад", "сад"),
                ("сельское поселение", "с/п"), ("Километр", "км"), ("Бульвар", "б-р"), ("дачный поселок", "дп"), ("Проспект", "пр-кт"),
                ("Площадь", "пл"), ("гаражно-строительный кооператив", "гск"), ("Тупик", "туп"), ("строение", "стр"), ("заимка", "заимка"),
                ("Набережная", "наб"), ("починок", "починок"), ("участок", "уч-к"), ("аллея", "аллея"), ("просека", "просека"),
                ("зона", "зона"), ("железнодорожная станция", "ж/д_ст"), ("въезд", "въезд"), ("железнодорожная казарма", "ж/д_казарм"),
                ("парк", "парк"), ("Область", "обл"), ("площадка", "пл-ка"), ("поселок и(при) станция(и)", "п/ст"), ("Тракт", "тракт"),
                ("ферма", "ферма"), ("слобода", "сл"), ("животноводческая точка", "жт"), ("остров", "остров"), ("железнодорожная будка", "ж/д_будка"),
                ("дорога", "дор"), ("казарма", "казарма"), ("железнодорожная платформа", "ж/д_платф"), ("городок", "городок"),
                ("промышленная зона", "промзона"), ("железнодорожный разъезд", "ж/д_рзд"), ("курортный поселок", "кп"),
                ("выселки(ок)", "высел"), ("улус", "у"), ("Переезд", "переезд"), ("просек", "просек"), ("железнодорожный пост", "ж/д_пост"),
                ("ж/д остановочный (обгонный) пункт", "ж/д_оп"), ("жилой район", "жилрайон"), ("сквер", "сквер"), ("сельское муницип. образование", "с/мо"),
                ("абонентский ящик", "а/я"), ("планировочный район", "п/р"), ("заезд", "заезд"), ("почтовое отделение", "п/о"),
                ("спуск", "спуск"), ("массив", "массив"), ("автодорога", "автодорога"), ("платформа", "платф"), ("кольцо", "кольцо"),
                ("проулок", "проулок"), ("леспромхоз", "лпх"), ("Автономный округ", "АО"), ("аал", "аал"), ("Край", "край"),
                ("мост", "мост"), ("Республика", "Респ"), ("ряды", "ряды"), ("сельская администрация", "с/а"), ("кордон", "кордон"),
                ("вал", "вал"), ("канал", "канал"), ("полустанок", "полустанок"), ("проселок", "проселок"), ("округ", "округ"),
                ("арбан", "арбан"), ("погост", "погост"), ("сельский округ", "с/о"), ("проток", "проток"), ("бугор", "бугор"),
                ("жилзона", "жилзона"), ("коса", "коса"), ("Автономная область", "Аобл"), ("Чувашская автономная область", "Чувашия"),
                ("дачное некомерческое партнерство", "днп"), ("фермерское хозяйство", "ф/х"), ("некоммерческое партнерство", "н/п"),
                ("местность", "местность"), ("балка", "балка"), ("бухта", "бухта"), ("эстакада", "эстакада"), ("маяк", "маяк"),
                ("горка", "горка"), ("тоннель", "тоннель"), ("мыс", "мыс");
        ');
        $this->_message('Migration apply!');
    }

    public function migration2()
    {
        $db = DB::getInstance();
        $db->exec('ALTER TABLE a_object ADD KEY `k_a_object1` (`aolevel`)');
        $db->exec('ALTER TABLE a_object ADD KEY `k_a_object2` (`shortname`)');
        $db->exec('ALTER TABLE a_object ADD KEY `k_a_object3` (`regioncode`)');
        $db->exec('ALTER TABLE a_object ADD KEY `k_a_object4` (`parentguid`)');
        $db->exec('ALTER TABLE a_object ADD KEY `k_a_object5` (`actstatus`)');
        $db->exec('ALTER TABLE a_object ADD KEY `k_a_object6` (`startdate`)');
        $db->exec('ALTER TABLE a_object ADD KEY `k_a_object7` (`enddate`)');
        $db->exec('ALTER TABLE a_house ADD KEY `k_a_house1` (`aoguid`)');
        $db->exec('ALTER TABLE a_house ADD KEY `k_a_house2` (`startdate`)');
        $db->exec('ALTER TABLE a_house ADD KEY `k_a_house3` (`enddate`)');
        $this->_message('Migration 2 apply!');
    }

    /**
     * Завершение parent process
     * @param int $c код выхода
     */
    private function _die($c)
    {
        // удалим PID файл
        if (is_readable($this->pidPath)) {
            unlink($this->pidPath);
        }
        exit($c);
    }

    /**
     * Генератор для прохода по частям файлов
     * @param bool $xml возвращает с расширением .xml или нет
     * @return Generator
     * $0 - bool признак, является ли файл первым из частей, $1 - имя файла
     */
    private function _listFiles($xml = false)
    {
        static $fileNames;
        if (empty($fileNames)) {
            $fileNames = [];
            $hd = opendir($this->basePath . '/uploads/');
            while (false !== ($entry = readdir($hd))) {
                if ($entry == '.' || $entry == '..') continue;
                if (false !== strpos($entry, '.XML')) {
                    $fileNames[] = substr($entry, 0, -4);
                }
            }
            closedir($hd);
        }
        $alp = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
        foreach ($fileNames as $entry) {
            $fisrt = 0;
            foreach ($alp as $x) {
                foreach ($alp as $y) {
                    $pre = $entry . '_' . $x . $y . ($xml ? '.xml' : '');
                    if (!is_file($this->basePath . '/uploads/pre/' . $pre)) {
                        break 2;
                    }
                    yield [(!$fisrt ? true : false), $pre];
                    ++$fisrt;
                }
            }
        }
    }

    private function _formate()
    {
        /** @var $db PDO */
        $db = DB::getInstance();
        $f = function () {
            // разбиваем на строки нужные тэги
            $this->_message('разбиваем на строки нужные тэги');
            foreach ($this->_listFiles() as list($a, $v)) {
                if ((false === strpos($v, 'AS_ADDROB')) && (false === strpos($v, 'AS_HOUSE_'))) {
                    continue;
                }
                if ($this->_runMainLoop) {
                    $workerPid = pcntl_fork();
                    if ($workerPid) {
                        $this->_workerPids[$workerPid] = 1;
                    } else {
                        $this->_parentProc = false;
                        // @todo: по идеи тоже бы замутить с помощью sed
                        $f1 = $this->basePath . '/uploads/pre/' . $v;
                        $f2 = $this->basePath . '/uploads/pre/' . $v . '.xml';
                        $fp1 = fopen($f1, 'r');
                        $fp2 = fopen($f2, 'a-');
                        while ($contents = fread($fp1, 1024)) {
                            $contents = str_replace(['<Object', '<House'], ["\n<Object", "\n<House"], $contents);
                            fputs($fp2, $contents);
                        }
                        fclose($fp1);
                        fclose($fp2);
                        exit(0);
                    }
                }
            }
            $this->_waitChild();
            exec("rename -f 's/.xml//g' " . $this->basePath . '/uploads/pre/*.xml');
            // лотаем прорехи в местах разреза
            $this->_message('лотаем прорехи в местах разреза');
            $last = $prev = '';
            foreach ($this->_listFiles() as list($a, $v)) {
                $file = $this->basePath . '/uploads/pre/' . $v;
                // Вставим если это не первая часть
                if (!$a) {
                    exec("echo '$last' > " . $file . '.xml && cat ' . $file . ' >> ' . $file . '.xml');
                    // стираем в предыдущей части последнюю строку
                    exec('sed \'$d\' ' . $prev . ' > ' . $prev . "_1 && rename -f 's/_1//g' " . $prev . '_1');
                }
                // Отрежем последнюю строку
                $out = [];
                exec('tail -n 1 ' . $file, $out, $r);
                $last = trim($out[0]);
                if (!$a) $prev = $file . '.xml';
                else $prev = $file;
            }
            // После остались как и старый вариант, так и правильные части с расширением .xml, вернем обратно имена файлов без расширения .xml
            $c = "rename -f 's/.xml//g' " . $this->basePath . '/uploads/pre/*.xml';
            system($c);
            $this->_message('First formate successful!');
        };
        $stm = $db->query('SELECT type_name, val FROM a_settings WHERE type_name IN("formate", "pre")');
        if ($stm->rowCount() < 2) {
            $row = $stm->fetch(PDO::FETCH_ASSOC);
            // если предыдущий шаг не выполнен, этот не стоит делать
            if ($row['type_name'] == 'formate') return false;
            $f();
            $db = DB::reConnect();
            $db->exec('INSERT INTO a_settings (type_name, val) VALUE ("formate", ' . $db->quote($row['val']) . ')');
            return true;
        } else {
            $arr = [];
            while ($row = $stm->fetch(PDO::FETCH_ASSOC)) $arr[$row['type_name']] = $row;
            if ($arr['formate']['val'] == $arr['pre']['val']) {
                $this->_message('First formate already!');
                return true;
            } else {
                $f();
                $db = DB::reConnect();
                $db->exec('UPDATE a_settings SET val = ' . $db->quote($arr['pre']['val']) . ' WHERE type_name = "formate" LIMIT 1');
                return true;
            }
        }
    }

    private function _parse()
    {
        // @todo: записывать каждый распарсенный файл в БД, чтобы можно было прерывать распарсевание на пол пути, например ставить лимит обрабатываемых файлов, тем самым разделять на части загрузку
        // пока ставим дату после загрузки всех файлов
        $parse = function() {
            foreach ($this->_listFiles() as list($f, $v)) {
                $t = '';
                if (false !== strpos($v, 'AS_ADDROB')) $t = 'obj';
                if (false !== strpos($v, 'AS_HOUSE_')) $t = 'house';
                if (empty($t)) continue;
                if ((count($this->_workerPids) < $this->workerCount) && $this->_runMainLoop) {
                    $workerPid = pcntl_fork();
                    if ($workerPid) {
                        $this->_workerPids[$workerPid] = 1;
                    } else {
                        $this->_parentProc = false;
                        $this->_parseFile($this->basePath . '/uploads/pre/' . $v, $t);
                        exit(0);
                    }
                } else {
                    $sPid = pcntl_waitpid(-1, $status, WUNTRACED);
                    unset($this->_workerPids[$sPid]);
                    if ($this->_runMainLoop) {
                        $workerPid = pcntl_fork();
                        if ($workerPid) {
                            $this->_workerPids[$workerPid] = 1;
                        } else {
                            $this->_parentProc = false;
                            $this->_parseFile($this->basePath . '/uploads/pre/' . $v, $t);
                            exit(0);
                        }
                    }
                }
            }
            $this->_waitChild();
            $this->_message('Parse successful!');
        };
        $db = DB::getInstance();
        $stm = $db->query('SELECT type_name, val FROM a_settings WHERE type_name IN ("parse", "formate")');
        if ($stm->rowCount() < 2) {
            $row = $stm->fetch(PDO::FETCH_ASSOC);
            // если предыдущий шаг не выполнен, этот не стоит делать
            if ($row['type_name'] == 'parse') return false;
            $parse();
            $db = DB::reConnect();
            $this->migration2();
            $db->exec('INSERT INTO a_settings (type_name, val) VALUE ("parse", ' . $db->quote($row['val']) . ')');
            return true;
        } else {
            $arr = [];
            while ($row = $stm->fetch(PDO::FETCH_ASSOC)) $arr[$row['type_name']] = $row;
            if ($arr['parse']['val'] == $arr['formate']['val']) {
                $this->_message('Parse already!');
                return true;
            } else {
                $parse();
                $db = DB::reConnect();
                $this->migration2();
                $db->exec('UPDATE a_settings SET val = ' . $db->quote($arr['formate']['val']) . ' WHERE type_name = "parse" LIMIT 1');
                return true;
            }
        }
    }

    /**
     * Функция ожидания завершения всех потомков
     * @return bool
     */
    private function _waitChild()
    {
        while ($sPid = pcntl_waitpid(-1, $status, WUNTRACED)) {
            if ($sPid == -1) {
                // рабочих процессов нет. выходим из цикла проверки
                $this->_workerPids = [];
                break;
            } else {
                // завершился процесс. удалим его PID
                unset($this->_workerPids[$sPid]);
            }
        }
        return true;
    }

    /**
     * @param string $file полный путь до файла
     * @param string $houseOrObj
     */
    private function _parseFile($file, $houseOrObj)
    {
        if (!($fp = fopen($file, 'r'))) {
            // Надо будет разбиратся в чем причина
            $this->_message('Error read file: ' . $file);
            return;
        }
        $this->_message('Parse file: ' . $file);
        $i = $c = 0;
        $arr = [];
        switch($houseOrObj) {
            case 'obj': {
                $tag = 'Object';
                $atr = [
                    'AOID'       => 'aoid',
                    'AOGUID'     => 'aoguid',
                    'PARENTGUID' => 'parentguid',
                    'PREVID'     => 'previd',
                    'NEXTID'     => 'nextid',
                    'FORMALNAME' => 'formalname',
                    'OFFNAME'    => 'offname',
                    'SHORTNAME'  => 'shortname',
                    'AOLEVEL'    => 'aolevel',
                    'REGIONCODE' => 'regioncode',
                    'AREACODE'   => 'areacode',
                    'AUTOCODE'   => 'autocode',
                    'CITYCODE'   => 'citycode',
                    'CTARCODE'   => 'ctarcode',
                    'PLACECODE'  => 'placecode',
                    'STREETCODE' => 'streetcode',
                    'EXTRCODE'   => 'extrcode',
                    'SEXTCODE'   => 'sextcode',
                    'PLAINCODE'  => 'plaincode',
                    'CODE'       => 'code',
                    'CURRSTATUS' => 'currstatus',
                    'ACTSTATUS'  => 'actstatus',
                    'LIVESTATUS' => 'livestatus',
                    'CENTSTATUS' => 'centstatus',
                    'OPERSTATUS' => 'operstatus',
                    'IFNSFL'     => 'ifnsfl',
                    'IFNSUL'     => 'ifnsul',
                    'TERRIFNSFL' => 'terrifnsfl',
                    'TERRIFNSUL' => 'terrifnsul',
                    'OKATO'      => 'okato',
                    'OKTMO'      => 'oktmo',
                    'POSTALCODE' => 'postalcode',
                    'STARTDATE'  => 'startdate',
                    'ENDDATE'    => 'enddate',
                    'UPDATEDATE' => 'updatedate',
                    'NORMDOC'    => 'normdoc'
                ];
                break;
            }
            default: {
                $tag = 'House';
                $atr = [
                    'HOUSEID'    => 'houseid',
                    'HOUSEGUID'  => 'houseguid',
                    'AOGUID'     => 'aoguid',
                    'HOUSENUM'   => 'housenum',
                    'BUILDNUM'   => 'buildnum',
                    'STRUCNUM'   => 'strucnum',
                    'STRSTATUS'  => 'strstatus',
                    'ESTSTATUS'  => 'eststatus',
                    'STATSTATUS' => 'statstatus',
                    'IFNSFL'     => 'ifnsfl',
                    'IFNSUL'     => 'ifnsul',
                    'TERRIFNSFL' => 'terrifnsfl',
                    'TERRIFNSUL' => 'terrifnsul',
                    'OKATO'      => 'okato',
                    'OKTMO'      => 'oktmo',
                    'POSTALCODE' => 'postalcode',
                    'STARTDATE'  => 'startdate',
                    'ENDDATE'    => 'enddate',
                    'UPDATEDATE' => 'updatedate',
                    'COUNTER'    => 'counter',
                    'NORMDOC'    => 'normdoc',
                ];
                break;
            }
        }
        while(($line = fgets($fp, 4096)) !== false) {
            // строки не содержащие нужного тэга пропускаем
            if (strpos($line, $tag) === false) continue;
            // место разрыва, надо дочитать еще строку
            if (strpos($line, '/>') === false) {
                $line .= fgets($fp, 4096);
            }
            $item = [];
            $patern = '#';
            foreach ($atr as $v) {
                $patern .= '(.*?(' . $v . '){1}="(.*?)")?';
            }
            $patern .= '#ism';
            if (preg_match($patern, $line, $match)) {
                $co = sizeof($match);
                for($j = 2; $j < $co; $j += 3) {
                    if (!empty($match[$j])) {
                        $item[$atr[$match[$j]]] = $match[$j + 1];
                    }
                }
            }
            $arr[] = $item;
            if ($i == 99) {
                $this->_insert($tag, $atr, $arr);
                $c += $i;
                $arr = [];
                $i = 0;
            }
            ++$i;
        }
        if ($i) {
            $this->_insert($tag, $atr, $arr);
            $c += $i;
        }
        $this->_message('Add (' . $c . ") '" . $tag . "' from file " . $file);
    }

    /**
     * @param string $tag что добавляем
     * @param array $atr ключи атрибутов
     * @param array $arr массив с элементами
     */
    private function _insert($tag, $atr, $arr)
    {
        static $query;
        if (empty($query[$tag])) {
            $query[$tag] = 'INSERT INTO ' . ($tag == 'Object' ? 'a_object' : 'a_house') . ' (`' . implode('`, `', $atr) . '`) VALUES ';
        }
        /** @var PDO $db */
        $db = DB::reConnect();
        $q = $query[$tag];
        $i = 0;
        foreach ($arr as $item) {
            if (!$i) ++$i;
            else $q .= ',';
            $q .= '(';
            $j = 0;
            foreach ($atr as $v) {
                if (!$j) ++$j;
                else $q .= ',';
                $q .= (!empty($item[$v]) ? $db->quote($item[$v]) : 'NULL');
            }
            $q .= ')';
        }
        if ($i) {
            try {
                $db->exec($q);
            } catch (PDOException $e) {
                $this->_message($q);
                $this->_message($e->getMessage());
            }
        }
    }

    /**
     * Пополнение таблицы сопоставления типов 'объектов'
     */
    private function _type()
    {
        /** @var $db \PDO */
        $db = DB::getInstance();
        $db->exec('REPLACE INTO a_settlement_type (shortname, fullname) SELECT short_name, `name` FROM settlement_type WHERE short_name IS NOT NULL AND short_name <> ""');
        $stmn = $db->query('SELECT t1.id, t1.`name`, t2.shortname
            FROM settlement_type AS t1 LEFT JOIN a_settlement_type AS t2 ON t2.fullname = t1.name
            WHERE short_name IS NULL OR short_name = ""');
        $q = '';
        $i = 0;
        while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
            if (!empty($row['shortname'])) {
                $q .= 'UPDATE settlement_type SET short_name = ' . $db->quote($row['shortname']) . ' WHERE id = ' . $row['id'] . ';';
                ++$i;
            }
        }
        if ($i) {
            $db->exec($q);
            $this->_message('Update (' . $i . ') settlement_type');
        }
        // Добавление новых типов
        // Добавим 'км'
        // $db->exec('INSERT INTO street_type (`name`, `short_name`) VALUES ("Километр", "км"), ("Парк", "парк"), ("Переезд", "переезд"), ("Разъезд", "рзд")');
        /*
        $stmn = $db->query('SELECT MAX(sort) FROM settlement_type');
        $sort = $stmn->fetchColumn();
        $i = ++$sort;
        $stmn = $db->query('SELECT * FROM a_settlement_type WHERE fullname NOT IN (SELECT `name` FROM settlement_type)');
        if ($stmn->rowCount() > 0) {
            $q = 'INSERT INTO settlement_type (`name`, `short_name`, `sort`) VALUES ';
            while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
                if ($i != $sort) {
                    $q .= ',';
                }
                $q .= '("' . $row['fullname'] . '", "' . $row['shortname'] . '", ' . $i . ')';
                ++$i;
            }
            $n = $db->exec($q);
            $this->_message('Insert (' . $n . ') settlement_type');
        }
        */
        $db->exec('REPLACE INTO a_settlement_type (shortname, fullname) SELECT short_name, `name` FROM settlement_district_type WHERE short_name IS NOT NULL AND short_name <> ""');
        $db->exec('REPLACE INTO a_settlement_type (shortname, fullname) SELECT short_name, `name` FROM street_type WHERE short_name IS NOT NULL AND short_name <> ""');
    }

    /**
     * Построение всей иеррархии от региона до улиц
     * @param int $region
     * @return bool
     */
    private function _settlementStreet($region)
    {
        /**
         * Сделаем допущение, что все регионы уже в БД, а еще берем только Курганскую область
         * aolevel = 4 это видимо Городские округа, это Курган и Щадринск
         * Еще р-н не берем
         */
        /** @var $db \PDO */
        $db = DB::getInstance();
        $stmn = $db->query('SELECT aoguid FROM a_object WHERE regioncode = ' . $region . ' AND aolevel = 1');
        $guid = $stmn->fetchColumn();
        // крупные города - городские округа
        $stmn = $db->query('SELECT aoguid, offname, shortname FROM a_object
            WHERE parentguid = "' . $guid . '" AND aolevel = 4 AND regioncode = ' . $region . ' AND actstatus = 1
                AND startdate <= CURDATE() AND enddate > CURDATE()');
        $cities = [];
        while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
            $cities[] = $row;
        }
        $this->_cities($cities, $region);
        // $this->_waitChild();
        $db = DB::reConnect();
        // пройдемся по р-нам, которые после области идут
        $stmn = $db->query('SELECT aoguid FROM a_object
            WHERE parentguid = "' . $guid . '" AND aolevel = 3 AND regioncode = ' . $region . ' AND actstatus = 1 AND startdate <= CURDATE() AND enddate > CURDATE()');
        $dis = [];
        while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
            $dis[] = $row['aoguid'];
        }
        foreach($dis as $v) {
            $this->_district($v, $region);
        }
        $this->_waitChild();
        $this->_message('Settlement and street successful!');
        return true;
    }

    /**
     * @param string $guid id р-н по версии fias
     * @param int $region
     */
    private function _district($guid, $region)
    {
        /** @var $db \PDO */
        $db = DB::reConnect();
        $limit = 50;
        // Города
        $stmn = $db->query('SELECT aoguid, offname, shortname FROM a_object
            WHERE parentguid = "' . $guid . '" AND regioncode = "' . $region . '" AND shortname = "г" AND actstatus = 1
                AND startdate <= CURDATE() AND enddate > CURDATE()');
        if ($stmn->rowCount() > 0) {
            $cities = [];
            while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
                $cities[] = $row;
            }
            $this->_cities($cities, $region);
        }
        // Деревни, поселки, села
        /*
        $o = 0;
        $arr = [];
        do {
            $db = DB::reConnect();
            $stmn = $db->query('SELECT aoguid, offname, shortname FROM a_object WHERE parentguid = "' . $guid . '" AND regioncode = "' . $region . '" AND aolevel = 6 LIMIT ' . $limit . ' OFFSET ' . $o);
            if (($r = $stmn->rowCount()) > 0) {
                while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
                    $arr[] = $row;
                }
                foreach ($arr as $v) {
                    if (count($this->_workerPids) < $this->workerCount) {
                        $workerPid = pcntl_fork();
                        if ($workerPid) {
                            $this->_workerPids[$workerPid] = 1;
                        } else {
                            $this->_parentProc = false;
                            $id = $this->_settlement($v['offname'], $region, $v['shortname']);
                            $this->_street($v['aoguid'], $id, $region);
                            exit(0);
                        }
                    } else {
                        $sPid = pcntl_waitpid(-1, $status, WUNTRACED);
                        unset($this->_workerPids[$sPid]);
                        $workerPid = pcntl_fork();
                        if ($workerPid) {
                            $this->_workerPids[$workerPid] = 1;
                        } else {
                            $this->_parentProc = false;
                            $id = $this->_settlement($v['offname'], $region, $v['shortname']);
                            $this->_street($v['aoguid'], $id, $region);
                            exit(0);
                        }
                    }
                }
            }
            $o += $limit;
        } while($r == $limit);
        */
    }

    /**
     * @param string $name
     * @param int $region
     * @param string $type сокращение
     * @return int id города
     */
    private function _settlement($name, $region, $type, $pid = 0)
    {
        /** @var $db \PDO */
        $db = DB::reConnect();
        $type = $this->_getType($type, 'settlement_type');
        // ищем у которого есть уже что-то или он включен
        $stmn = $db->query('SELECT t.* FROM (
            SELECT s1.id, s1.`name`, s1.available, count(s2.id) AS rrr
            FROM settlement AS s1 LEFT JOIN settlement AS s2 ON s2.parent_settlement_id = s1.id
            WHERE s1.`name` = "' . $name . '" AND s1.region_id = ' . $region . ' AND s1.type_id = ' . $type . ' GROUP BY s1.id, s1.`name`, s1.available
        ) AS t ORDER BY t.available DESC, t.rrr DESC LIMIT 1');
        if ($stmn->rowCount() > 0) {
            // город уже есть
            $id = $stmn->fetchColumn();
            $this->_message("Settlement '$name' already exists! id = $id");
        } else {
            $stmn = $db->prepare('INSERT INTO settlement (region_id, type_id, `name`, `parent_settlement_id`, `available`) VALUE (?, ?, ?, ?, 1)');
            $stmn->execute([$region, $type, $name, $pid]);
            $id = $db->lastInsertId();
            $this->_log("Add settlement '$name'! id = $id");
        }
        return $id;
    }

    /**
     * Деревни, поселки, районы и мкр.
     * @param $guid
     * @param $region
     * @return array
     */
    private function _settlementMore($guid, $region, $cityId)
    {
        /** @var $db \PDO */
        $db = DB::reConnect();
        $o = 0;
        $limit = 50;
        // Территории это типа тоже улицы
        /* отбрасываем пока, не известно что это такое
        do {
            $stmn = $db->query('SELECT aoid, aoguid, offname, shortname FROM a_object
                WHERE parentguid = "' . $guid . '" AND aolevel = 6 AND shortname IN ("тер") AND regioncode = "' . $region . '"
                ORDER BY aoid ASC LIMIT ' . $limit . ' OFFSET ' . $o);
            if (($r = $stmn->rowCount()) > 0) {
                while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
                    if (!($type = $this->_getType($row['shortname'], 'street_type'))) {
                        $this->_log("Street type undefined '" . $row['shortname'] . "'");
                        continue;
                    }
                    $stmn2 = $db->query('SELECT id FROM street WHERE `name` = "' . $row['offname'] . '" AND settlement_id = ' . $cityId . ' AND type_id = ' . $type);
                    if ($stmn2->rowCount() > 0) {
                        // уже есть обновим ее fias
                        $id = $stmn2->fetchColumn();
                        $db->exec('UPDATE street SET fias = "' . $row['aoguid'] . '" WHERE id = ' . $id);
                        $this->_log('Update street id: ' . $id);
                    } else {
                        // добавим улицу
                        $stmn2 = $db->prepare('INSERT INTO street (settlement_id, type_id, `name`, fias) VALUE (?, ?, ?, ?)');
                        $stmn2->execute([$cityId, $type, $row['offname'], $row['aoguid']]);
                        $this->_log("Add street '" . $row['offname'] . "'");
                    }
                }
            }
            $o += $limit;
        } while($r == $limit);
        */
        // Районы и мкр. города к ним улицы не привязаны, но к ним привязаны дома
        $o = 0;
        $ins = function($ra, $ma = []) use ($db) {
            $q = 'INSERT INTO settlement_district (settlement, `type`, `name`, short_name) VALUES ';
            $i = 0;
            $mess = [];
            foreach($ra as $v) {
                $type = $this->_getType($v[1], 'settlement_district_type');
                if (!empty($ma[$v[0]])) {
                    // типы не совпадают возможно это не он
                    if ($ma[$v[0]]['type'] != $type) {
                        if ($i) $q .= ',';
                        $q .= '(' . $v[2] . ', ' . ($type ? $type : 'NULL') . ', ' . $db->quote($v[0]) . ', ' . $db->quote($v[0]) . ')';
                        $mess[] = $v[0];
                        ++$i;
                    }
                } else {
                    if ($i) $q .= ',';
                    $q .= '(' . $v[2] . ', ' . ($type ? $type : 'NULL') . ', ' . $db->quote($v[0]) . ', ' . $db->quote($v[0]) . ')';
                    $mess[] = $v[0];
                    ++$i;
                }
            }
            if ($i) {
                try {
                    $db->exec($q);
                    $this->_message('Add settlement_district (' . $i . "): '" . implode("', '", $mess) . "'");
                } catch(PDOException $e) {
                    $this->_message($q);
                    $this->_message($e->getMessage());
                }
            }
        };
        do {
            $q = 'SELECT aoid, aoguid, offname, shortname FROM a_object
                WHERE parentguid = "' . $guid . '" AND aolevel = 6 AND shortname IN ("мкр", "жилрайон", "р-н") AND regioncode = "' . $region . '"
            AND actstatus = 1 AND startdate <= CURDATE() AND enddate > CURDATE() AND nextid IS NULL
                ORDER BY aoid ASC LIMIT ' . $limit . ' OFFSET ' . $o;
            $stmn = $db->query($q);
            if (($r = $stmn->rowCount()) > 0) {
                $ra = [];
                while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
                    $ra[] = [$row['offname'], $row['shortname'], $cityId];
                }
                // достанем сразу все районы привязанные к данному нп
                $stmn = $db->query('SELECT id, `name`, `type` FROM settlement_district WHERE settlement = ' . $cityId);
                if ($stmn->rowCount() > 0) {
                    $ma = [];
                    while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
                        $ma[$row['name']] = $row;
                    }
                    $ins($ra, $ma);
                } else {
                    // нет еще не одного района, добавим все
                    $ins($ra);
                }
            }
            $o += $limit;
        } while($r == $limit);
        // @todo: проверить есть ли у мкр. улицы, которые не включены в город
        // Улицы, переулки и т.п.
        $o = 0;
        do {
            $stmn = $db->query('SELECT aoid, aoguid, offname, shortname FROM a_object
                WHERE parentguid = "' . $guid . '" AND aolevel = 7 AND regioncode = "' . $region . '" AND actstatus = 1
                    AND startdate <= CURDATE() AND enddate > CURDATE()
                ORDER BY aoid ASC LIMIT ' . $limit . ' OFFSET ' . $o);
            if (($r = $stmn->rowCount()) > 0) {
                while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
                    if (!($type = $this->_getType($row['shortname'], 'street_type'))) {
                        $this->_log("Street type undefined '" . $row['shortname'] . "'");
                        continue;
                    }
                    $stmn2 = $db->query('SELECT id FROM street WHERE `name` = "' . $row['offname'] . '" AND settlement_id = ' . $cityId . ' AND type_id = ' . $type);
                    if ($stmn2->rowCount() > 0) {
                        // уже есть обновим ее fias
                        $id = $stmn2->fetchColumn();
                        $db->exec('UPDATE street SET fias = "' . $row['aoguid'] . '" WHERE id = ' . $id);
                        $this->_log('Update street id: ' . $id);
                    } else {
                        // добавим улицу
                        $stmn2 = $db->prepare('INSERT INTO street (settlement_id, type_id, `name`, fias) VALUE (?, ?, ?, ?)');
                        $stmn2->execute([$cityId, $type, $row['offname'], $row['aoguid']]);
                        $id = $db->lastInsertId();
                        $this->_log("Add street '" . $row['offname'] . "' id = " . $id);
                    }
                }
            }
            $o += $limit;
        } while($r == $limit);
        // Поселки и т.п. у них нет district, они типа тоже settlement
        $arr = [];
        /*
        $o = 0;
        do {
            $stmn = $db->query('SELECT aoid, aoguid, offname, shortname FROM a_object
                WHERE parentguid = "' . $guid . '" AND aolevel = 6 AND shortname NOT IN ("мкр", "жилрайон", "р-н", "тер") AND regioncode = "' . $region . '"
                ORDER BY aoid ASC LIMIT ' . $limit . ' OFFSET ' . $o);
            if (($r = $stmn->rowCount()) > 0) {
                while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
                    $id = $this->_settlement($row['offname'], $region, $row['shortname'], $cityId);
                    $arr[] = array_merge(['settlementId' => $id], $row);
                }
            }
            $o += $limit;
        } while($r == $limit);
        */
        return $arr;
    }

    /**
     * @param $guid
     * @param $settId
     */
    private function _street($guid, $settId, $region)
    {
        /** @var $db \PDO */
        $db = DB::reConnect();
        $o = 0;
        $limit = 50;
        do {
            $stmn = $db->query('SELECT aoid, aoguid, offname, shortname FROM a_object
                WHERE parentguid = "' . $guid . '" AND aolevel = 7 AND regioncode = "' . $region . '"
                    AND actstatus = 1 AND startdate <= CURDATE() AND enddate > CURDATE()
                ORDER BY aoid ASC LIMIT ' . $limit . ' OFFSET ' . $o);
            if (($r = $stmn->rowCount()) > 0) {
                while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
                    if (!($type = $this->_getType($row['shortname'], 'street_type'))) {
                        $this->_log("Street type undefined '" . $row['shortname'] . "'");
                        continue;
                    }
                    $stmn2 = $db->query('SELECT id FROM street WHERE `name` = "' . $row['offname'] . '" AND settlement_id = ' . $settId . ' AND type_id = ' . $type);
                    if ($stmn2->rowCount() > 0) {
                        // уже есть обновим ее fias
                        $id = $stmn2->fetchColumn();
                        $db->exec('UPDATE street SET fias = "' . $row['aoguid'] . '" WHERE id = ' . $id);
                        $this->_log('Update street id: ' . $id);
                    } else {
                        // добавим улицу
                        $stmn2 = $db->prepare('INSERT INTO street (settlement_id, type_id, `name`, fias) VALUE (?, ?, ?, ?)');
                        $stmn2->execute([$settId, $type, $row['offname'], $row['aoguid']]);
                        $id = $db->lastInsertId();
                        $this->_log("Add street '" . $row['offname'] . "' id = " . $id);
                    }
                }
            }
            $o += $limit;
        } while($r == $limit);
    }

    /**
     * Определение типа
     * @param string $shortname
     * @param string $from где искать
     * @return int
     */
    private function _getType($shortname, $from)
    {
        static $arr;
        if (empty($arr[$shortname])) {
            /** @var $db \PDO */
            $db = DB::getInstance();
            $stmn = $db->query('SELECT id FROM `' . $from .'` WHERE `name` IN (SELECT fullname FROM a_settlement_type WHERE shortname = "' . $shortname . '") LIMIT 1');
            if ($stmn->rowCount() > 0) {
                $arr[$shortname] = $id = $stmn->fetchColumn();
                return $id;
            } else {
                return 0;
            }
        } else {
            return $arr[$shortname];
        }
    }

    /**
     * @param array $cities массив городов
     * @param int $region
     */
    private function _cities($cities, $region)
    {
        foreach ($cities as $v) {
            if ((count($this->_workerPids) < $this->workerCount) && $this->_runMainLoop) {
                $workerPid = pcntl_fork();
                if ($workerPid) {
                    $this->_workerPids[$workerPid] = 1;
                } else {
                    $this->_parentProc = false;
                    $id = $this->_settlement($v['offname'], $region, $v['shortname']);
                    $arr = $this->_settlementMore($v['aoguid'], $region, $id);
                    $arr[] = array_merge(['settlementId' => $id], $v);
                    foreach($arr as $s) {
                        $this->_street($s['aoguid'], $s['settlementId'], $region);
                    }
                    exit(0);
                }
            } else {
                $sPid = pcntl_waitpid(-1, $status, WUNTRACED);
                unset($this->_workerPids[$sPid]);
                if ($this->_runMainLoop) {
                    $workerPid = pcntl_fork();
                    if ($workerPid) {
                        $this->_workerPids[$workerPid] = 1;
                    } else {
                        $this->_parentProc = false;
                        $id = $this->_settlement($v['offname'], $region, $v['shortname']);
                        $arr = $this->_settlementMore($v['aoguid'], $region, $id);
                        $arr[] = array_merge(['settlementId' => $id], $v);
                        foreach($arr as $s) {
                            $this->_street($s['aoguid'], $s['settlementId'], $region);
                        }
                        exit(0);
                    }
                }
            }
        }

    }
    /**
     * Непосредственно перенос самих домов
     */
    private function _house($region)
    {
        /** @var $db \PDO */
        $o = 0;
        $limit = 10;
        do {
            $db = DB::reConnect();
            // берем только города
            $q = 'SELECT s.id, s.name, "город" AS type FROM settlement AS s
                WHERE s.region_id = ' . $region . ' AND s.type_id = 1 ORDER BY s.id ASC LIMIT ' . $limit . ' OFFSET ' . $o;
            $stmn = $db->query($q);
            if (($r = $stmn->rowCount()) > 0) {
                $arr = [];
                while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
                    $arr[] = $row;
                }
                foreach($arr as $v) {
                    if ((count($this->_workerPids) < $this->workerCount) && $this->_runMainLoop) {
                        $workerPid = pcntl_fork();
                        if ($workerPid) {
                            $this->_workerPids[$workerPid] = 1;
                        } else {
                            $this->_parentProc = false;
                            $this->_houseProccess($v['id'], $v['name'], $v['type'], $region);
                            exit(0);
                        }
                    } else {
                        $sPid = pcntl_waitpid(-1, $status, WUNTRACED);
                        unset($this->_workerPids[$sPid]);
                        if ($this->_runMainLoop) {
                            $workerPid = pcntl_fork();
                            if ($workerPid) {
                                $this->_workerPids[$workerPid] = 1;
                            } else {
                                $this->_parentProc = false;
                                $this->_houseProccess($v['id'], $v['name'], $v['type'], $region);
                                exit(0);
                            }
                        }
                    }
                }
            }
            $o += $limit;
        } while($r == $limit);
        $this->_waitChild();
        $this->_message('House already!');
    }
    private function _houseProccess($settId, $settName, $settType, $region)
    {
        $o = 0;
        $limit = 10;
        do {
            /** @var PDO $db */
            $db = DB::reConnect();
            $stmn = $db->query('SELECT id, `name`, fias FROM street WHERE fias IS NOT NULL AND fias <> "" AND settlement_id = ' . $settId . '
                ORDER BY id LIMIT ' . $limit . ' OFFSET ' . $o);
            if (($r = $stmn->rowCount()) > 0) {
                $street = $fs = [];
                while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
                    $street[] = $row;
                    $fs[] = $row['fias'];
                }
                // пренадлежит ли улица какому-нибудь мкр.
                $mkr = $this->_disExist($fs, $settId);
                unset($fs);
                foreach ($street as $v) {
                    $o2 = 0;
                    $limit2 = 20;
                    do {
                        $stmn = $db->query('SELECT houseid, housenum, buildnum, strucnum FROM a_house
                            WHERE aoguid = "' . $v['fias'] . '" AND startdate <= CURDATE() AND enddate > CURDATE()
                            ORDER BY houseid LIMIT ' . $limit2 . ' OFFSET ' . $o2);
                        if (($r2 = $stmn->rowCount()) > 0) {
                            while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
                                if (!empty($row['buildnum']) && !empty($row['strucnum'])) {
                                    $corps = 'К' . $row['buildnum'] . $row['strucnum'];
                                } elseif(!empty($row['buildnum'])) {
                                    $corps = 'К' . $row['buildnum'];
                                } elseif(!empty($row['strucnum'])) {
                                    $corps = $row['strucnum'];
                                } else {
                                    $corps = '';
                                }
                                $housenum = $row['housenum'] . $corps;
                                // проверяем есть ли такой дом
                                $stmn2 = $db->query('SELECT id, coords FROM house WHERE street_id = ' . $v['id'] . ' AND house = ' . $db->quote($housenum) . ' AND region_id = ' . $region);
                                if ($stmn2->rowCount() > 0) {
                                    // есть такой
                                    $house = $stmn2->fetch(PDO::FETCH_ASSOC);
                                    if (empty($house['coords'])) {
                                        // обновим координаты с карты
                                        if (!empty($mkr[$v['fias']])) {
                                            // есть район города, ищем по нему
                                            $address = $this->_getRaion($region, $mkr[$v['fias']]['aoguid']);
                                        } else {
                                            // ищем по улице
                                            $address = $this->_getRaion($region, $v['fias']);
                                        }
                                        $address .= $settType . ' ' . $settName . ', ' . $v['name'] . ' ' . $row['housenum'] . (!empty($row['buildnum']) ? ' корпус ' . $row['buildnum'] : '') . (!empty($row['strucnum']) ? ' строение ' . $row['strucnum'] : '');
                                        $coords = $this->_coords($region, $address);
                                        $q = 'UPDATE house SET coords = "' . $coords . '" WHERE id = ' . $house['id'];
                                        $this->_log('Update coords house id = ' . $house['id']);
                                        $db->exec($q);
                                    }
                                } else {
                                    // добавим
                                    // обновим координаты с карты
                                    if (!empty($mkr[$v['fias']])) {
                                        // есть район города, ищем по нему
                                        $address = $this->_getRaion($region, $mkr[$v['fias']]['aoguid']);
                                    } else {
                                        // ищем по улице
                                        $address = $this->_getRaion($region, $v['fias']);
                                    }
                                    $address .= $settType . ' ' . $settName . ', ' . $v['name'] . ' ' . $row['housenum'] . ($corps ? ' корпус ' . $corps : '');
                                    $coords = $this->_coords($region, $address);
                                    $typeId = $this->_getTypeHouse();
                                    if (!empty($mkr[$v['fias']])) {
                                        if ($mkr[$v['fias']]['type'] == 1) {
                                            // район
                                            $q = 'INSERT INTO house (street_id, district_id, settlement_id, region_id, house, corps, type_id, hits, coords) VALUE
                                            (' . $v['id'] . ', ' . $mkr[$v['fias']]['id'] . ', ' . $settId . ', ' . $region . ', ' . $db->quote($housenum) . ', "", ' . $typeId . ', 0, ' . $db->quote($coords) . ')';
                                        } else {
                                            // мкр.
                                            $q = 'INSERT INTO house (street_id, microdistrict_id, settlement_id, region_id, house, corps, type_id, hits, coords) VALUE
                                            (' . $v['id'] . ', ' . $mkr[$v['fias']]['id'] . ', ' . $settId . ', ' . $region . ', ' . $db->quote($housenum) . ',"", ' . $typeId . ', 0, ' . $db->quote($coords) . ')';
                                        }
                                    } else {
                                        $q = 'INSERT INTO house (street_id, settlement_id, region_id, house, corps, type_id, hits, coords) VALUE
                                            (' . $v['id'] . ', ' . $settId . ', ' . $region . ', ' . $db->quote($housenum) . ', "", ' . $typeId . ', 0, ' . $db->quote($coords) . ')';
                                    }
                                    $db->exec($q);
                                    $id = $db->lastInsertId();
                                    $this->_log('Add house id = ' . $id);
                                }
                            }
                        }
                        $o2 += $limit2;
                    } while($r2 == $limit2);
                }
                // QUINCY JONES - Soul Bossa Nova
            }
            $o += $limit;
        } while($r == $limit);
    }
    private function _getDis($settId)
    {
        static $arr;
        if (empty($arr[$settId])) {
            /** @var PDO $db */
            $db = DB::getInstance();
            $stmn = $db->query('SELECT id, settlement, `name`, `type` FROM settlement_district WHERE settlement = ' . $settId);
            while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
                $arr[$settId][$row['name']] = $row;
            }
        }
        return (!empty($arr[$settId]) ? $arr[$settId] : []);
    }
    private function _disExist($fs, $settId)
    {
        $arr = [];
        /** @var PDO $db */
        $db = DB::getInstance();
        $stmn = $db->query('SELECT aoguid, parentguid FROM a_object WHERE aoguid IN("' . implode('","', $fs) . '")');
        if ($stmn->rowCount() > 0) {
            $a = [];
            while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
                $a[$row['aoguid']] = $row['parentguid'];
            }
            $q = '';
            $i = 0;
            foreach ($a as $k => $v) {
                if ($i) $q .= ' UNION ALL ';
                $q .= 'SELECT aoguid, offname, shortname, "' . $k . '" AS fias_street FROM a_object WHERE aoguid = "' . $v . '" AND shortname IN ("мкр", "р-н", "жилрайон")';
                ++$i;
            }
            $stmn = $db->query($q);
            if ($stmn->rowCount() > 0) {
                $mkr = [];
                while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
                    $type = $this->_getType($row['shortname'], 'settlement_district_type');
                    $mkr[$row['offname']] = array_merge(['type' => $type], $row);
                }
                foreach ($this->_getDis($settId) as $k => $v) {
                    if(!empty($mkr[$k]) && ($mkr[$k]['type'] == $v['type'])) {
                        $arr[$v['fias_street']] = ['id' => $v['id'], 'type' => $v['type'], 'aoguid' => $v['aoguid']];
                    }
                }
            }
        }
        return $arr;
    }

    /**
     * Простой GET запрос без особых заголовков
     * @param string $url
     * @return string
     */
    private function _send($url)
    {
        $arr['http']['method'] = 'GET';
        $arr['http']['header'] = 'Connection:close';
        return file_get_contents($url, false, stream_context_create($arr));
    }

    /**
     * @param string $address
     * @return string
     */
    private function _coords($region, $address) {
        $obl = [
            45 => 'Курганская+область'
        ];
        $address = str_replace(' ', '+', $address);
        $url = 'http://geocode-maps.yandex.ru/1.x/?format=json&results=1&geocode=Россия,+' . $obl[$region] . ',+' . $address;
        $res = $this->_send($url);
        $this->_log("Request coords from yandex '$address'");
        if (!empty($res)) {
            $res = json_decode($res, true);
            if (!empty($res['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'])) {
                $coords = explode(' ', $res['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']);
                return $coords[0] . ',' . $coords[1];
            }
        }
        return '';
    }

    /**
     * Ищем район области
     * @param int $region
     * @param string $guid
     * @return string
     */
    private function _getRaion($region, $guid)
    {
        /** @var PDO $db */
        $db = DB::getInstance();
        $stmn = $db->query('SELECT parentguid FROM a_object WHERE aoguid = "' . $guid . '" AND regioncode = "' . $region . '"');
        $p = $stmn->fetchColumn();
        $stmn = $db->query('SELECT offname FROM a_object
            WHERE aoguid = "' . $p . '" AND regioncode = "' . $region . '" AND shortname = "р-н" AND aolevel = 3
                AND actstatus = 1 AND startdate <= CURDATE() AND enddate > CURDATE() LIMIT 1');
        if ($stmn->rowCount() > 0) {
            $name = $stmn->fetchColumn();
            return $name . ' район';
        } else {
            return '';
        }
    }

    private function _transfer($region)
    {
        /** @var PDO $db */
        $db = DB::getInstance();
        $stm = $db->query('SELECT type_name, val FROM a_settings WHERE type_name IN ("transfer", "parse")');
        if ($stm->rowCount() < 2) {
            $row = $stm->fetch(PDO::FETCH_ASSOC);
            // если предыдущий шаг не выполнен, этот не стоит делать
            if ($row['type_name'] == 'transfer') return false;
            if ($this->_settlementStreet($region)) {
                $this->_house($region);
            }
            $db = DB::reConnect();
            $db->exec('INSERT INTO a_settings (type_name, val) VALUE ("transfer", "' . $row['val'] . '")');
            $this->_message('Transfer successful!');
            return true;
        } else {
            $arr = [];
            while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
                $arr[$row['type_name']] = $row;
            }
            if ($arr['transfer']['val'] == $arr['db_date']['val']) {
                $this->_message('Transfer already!');
                return true;
            } else {
                if ($this->_settlementStreet($region)) {
                    $this->_house($region);
                }
                $db = DB::reConnect();
                $db->exec('UPDATE a_settings SET val = "' . $arr['db_date']['val'] . '" WHERE type_name = "transfer" LIMIT 1');
                $this->_message('Transfer successful!');
                return true;
            }
        }
    }
    private function _getTypeHouse()
    {
        return 1;
    }

    /**
     * Инфа по проект - карте из 2гис
     * Возвращает те города, для которых существует карта в 2гис
     * @param array $arr название городов
     * @return array
     */
    private function _2gisProject($arr)
    {
        $out = [];
        $res = $this->_send('http://catalog.api.2gis.ru/project/list?version=1.3&key=' . $this->api2gisKey);
        if (!empty($res)) {
            $res = json_decode($res, true);
            foreach ($res['result'] as $v) {
                if (in_array($v['name'], $arr)) {
                    $out[] = $v['name'];
                }
            }
        }
        foreach ($arr as $k => $v) {
            if (!in_array($v, $out)) {
                unset($arr[$k]);
            }
        }
        return $arr;
    }
    private function _2gis($region)
    {
        $rubrics = [
            'Аварийные службы',
            'Справочно-информационные услуги',
            'Эвакуация автомобилей',
            'Управление ГО и ЧС',
            'Пожарная охрана',
            'Скорая медицинская помощь',
            'Независимые службы аварийных комиссаров',
            'Штрафстоянки',
            'Службы спасения',
            'Телефоны доверия',
            'Автозапчасти для иномарок',
            'Авторемонт и техобслуживание (СТО)',
            'Шины / диски',
            'Автомойки',
            'Шиномонтаж',
            'Автозапчасти для отечественных автомобилей',
            'Услуги авторазбора',
            'Автохимия / масла',
            'Автосигнализации - продажа / установка',
            'Автозаправочные станции (АЗС)',
            'Автоаксессуары',
            'Автомобильные аккумуляторы',
            'Технический осмотр транспорта',
            'Ремонт автоэлектрики',
            'Газовое оборудование для автотранспорта',
            'Кузовной ремонт',
            'Автозапчасти для грузовых автомобилей',
            'Специализированное автооборудование',
            'Автозвук',
            'Развал / Схождение',
            'Автостоянки',
            'Ремонт ходовой части автомобиля',
            'Ремонт карбюраторов / инжекторов',
            'Ремонт электронных систем управления автомобиля',
            'Установка / ремонт автостёкол',
            'Ремонт бензиновых двигателей',
            'Автостекло',
            'Тюнинг',
            'Аппаратная замена масла',
            'Запчасти к сельхозтехнике',
            'Тонирование автостёкол',
            'Автоэмали',
            'Выездная техническая помощь на дороге',
            'Ремонт',
            'Службы экстренного вызова',
            'Автозвук - продажа / установка',
            'Авторазборы',
            'Автохимия / Масла',
            'Автошины / Диски',
            'Антикоррозийная обработка автомобилей',
            'Аэрография на транспорте',
            'Запчасти для общественного транспорта',
            'Запчасти для спецавтотехники',
            'Контрактные автозапчасти',
            'Пункты технического осмотра транспорта',
            'Ремонт / заправка автокондиционеров',
            'Ремонт АКПП',
            'Ремонт дизельных двигателей',
            'Ремонт мототехники',
            'Ремонт спецавтотехники',
            'Ремонт топливной аппаратуры дизельных двигателей',
            'Администрации районов',
            'Администрация города Калтан',
            'Администрация города Киселёвска',
            'Администрация города Новокузнецка',
            'Администрация города Осинники',
            'Администрация города Прокопьевска',
            'Благотворительные фонды',
            'Военкоматы',
            'ГИБДД',
            'Гидрометеослужба',
            'Государственные архивы',
            'Детские дома / Приюты',
            'Дома престарелых',
            'Дома ребёнка',
            'ЗАГСы',
            'Законодательная власть',
            'Избирательные комиссии',
            'Изоляторы временного содержания',
            'Инспекции',
            'Исправительные учреждения',
            'Налоговые инспекции',
            'Общественные организации',
            'ОМВД / УМВД / ГУМВД / МВД',
            'Организации природоохраны',
            'Органы государственного надзора',
            'Отделения полиции',
            'Отделы ветеринарно-санитарного контроля',
            'Пенсионные фонды',
            'Политические организации',
            'Прокуратура',
            'Службы занятости населения',
            'Службы судебных приставов',
            'Социальные службы',
            'Судебная экспертиза',
            'Суды',
            'Таможня',
            'Участковые пункты полиции',
            'Федеральные казначейства',
            'Федеральные миграционные службы',
            'Федеральные службы',
            'Фонд обязательного медицинского страхования',
            'Фонды социального страхования',
            'Бани / Сауны',
            'Банкетные залы',
            'Бары',
            'Бильярдные залы',
            'Боулинг',
            'Букмекерские конторы / Лотереи',
            'Дайвинг-центры',
            'Детские / подростковые клубы',
            'Дома / дворцы культуры',
            'Доставка готовых блюд',
            'Кафе',
            'Кафе / рестораны быстрого питания',
            'Кафе-кондитерские / Кофейни',
            'Кейтеринг',
            'Кинотеатры',
            'Летний бум с ГИС',
            'Ночные клубы',
            'Организация и проведение праздников',
            'Организация театральных / концертных мероприятий',
            'Организация экскурсий по городу',
            'Парки культуры и отдыха',
            'Пляжи',
            'Праздник с ГИС',
            'Продажа лотерейных билетов',
            'Рестораны',
            'Салюты',
            'Спортивно-тактические клубы',
            'Столовые',
            'Стрелковые клубы',
            'Суши-бары / рестораны',
            'Творческие коллективы',
            'Тимбилдинг',
            'Услуги праздничного оформления',
            'Цирк',
            'IP-телефония',
            'Автоматизация бизнес-процессов',
            'Автоматизация производственных процессов',
            'Автоматизация торговли',
            'Аксессуары к мобильным телефонам',
            'Антенное оборудование',
            'Бухгалтерские программы',
            'Городские информационные сайты',
            'Государственные информационные сайты',
            'Интернет-провайдеры',
            'Информационная безопасность',
            'Коммутационное оборудование',
            'Компьютерные клубы',
            'Мобильные телефоны',
            'Монтаж / обслуживание антенного оборудования',
            'Монтаж телефонных сетей',
            'Навигационное оборудование',
            'Операторы кабельного телевидения',
            'Операторы сотовой связи',
            'Отделения электросвязи',
            'Офисные АТС',
            'Почтовые отделения',
            'Правовые программы',
            'Продажа программного обеспечения',
            'Разработка / поддержка / продвижение web-сайтов',
            'Ремонт мобильных устройств связи',
            'Спутниковая связь',
            'Средства радиосвязи',
            'Строительство / проектирование объектов связи',
            'Телефоны / Радиотелефоны',
            'Услуги телефонной связи',
            'Хостинг',
            'Ателье меховые / кожаные',
            'Ателье трикотажные',
            'Ателье швейные',
            'Благоустройство улиц',
            'Дезинфекция / Дератизация / Дезинсекция',
            'Домашний персонал',
            'ЖКХ',
            'ЖЭУ',
            'Изготовление ключей',
            'Кладбища',
            'Крематории',
            'Общежития',
            'Памятники / Ритуальные принадлежности',
            'Помощь в организации похорон',
            'Прачечные',
            'Продажа / обслуживание подъёмно-пассажирских устройств',
            'Профессиональная уборка',
            'Пункты приёма платежей / Расчётные центры коммунальных услуг',
            'Ремонт очков',
            'Ремонт часов',
            'Службы знакомств',
            'Теплоэнергоснабжение / ТЭЦ',
            'Техобслуживание теплоэнергосетей',
            'ТСЖ',
            'Услуги вышивки',
            'Услуги сурдопереводчиков',
            'Фото на документы',
            'Фотоцентры',
            'Химчистки одежды / текстиля',
            'Чистка / реставрация пухо-перьевых изделий',
            'CD / DVD / BD',
            'Аудио- / видеотехника',
            'Бытовая техника',
            'Заправка картриджей',
            'Запчасти / аксессуары для бытовой техники',
            'Компьютеры / Комплектующие',
            'Модернизация компьютеров',
            'Монтаж компьютерных сетей',
            'Музыкальные инструменты / Аксессуары',
            'Оргтехника',
            'Продажа / аренда / ремонт кофемашин',
            'Продажа кулеров для воды',
            'Расходные материалы для оргтехники',
            'Ремонт аудио / видео / цифровой техники',
            'Ремонт бытовой техники',
            'Ремонт компьютеров',
            'Ремонт оргтехники',
            'Сетевое оборудование',
            'Услуги системного администрирования',
            'Фототовары',
            'Антиквариат',
            'Библиотеки',
            'Воскресные школы',
            'Духовные учебные заведения',
            'Мечети',
            'Музеи',
            'Планетарий',
            'Приходы',
            'Религиозные организации',
            'Религиозные товары',
            'Театры',
            'Храмы / Соборы / Церкви',
            'Художественные выставки / Галереи',
            'Художественные мастерские',
            'Художественные салоны',
            'Часовни',
            'Детская мебель',
            'Изготовление мебели под заказ',
            'Изделия из камня для помещений',
            'ЛДСП / ДВПО / МДФ',
            'Матрасы',
            'Мебель для ванных комнат',
            'Мебель для кухни',
            'Мебель для медицинских учреждений и лабораторий',
            'Мебель для предприятий общественного питания',
            'Мебель для учебных и дошкольных учреждений',
            'Мебель из стекла',
            'Мебель корпусная',
            'Мебель мягкая',
            'Мебель офисная',
            'Мебельная фурнитура',
            'Мебельные ткани',
            'Мебельные фасады',
            'Металлическая мебель для помещений',
            'Облицовочные материалы для мебели',
            'Плетёная мебель / изделия',
            'Ремонт / реставрация мебели',
            'Садово-парковая мебель / Аксессуары',
            'Сейфы',
            'Серийное производство мебели',
            'Аптеки',
            'Биологически активные добавки (БАД)',
            'Больницы',
            'Бюро медико-социальной экспертизы',
            'Врачебные амбулатории',
            'Гирудотерапия',
            'Диагностические центры',
            'Диспансеры',
            'Женские консультации',
            'Инфракрасные сауны',
            'Коррекция зрения / Лечение офтальмологических заболеваний',
            'Косметика / Парфюмерия',
            'Косметика / расходные материалы для салонов красоты',
            'Косметика ручной работы',
            'Косметические услуги',
            'Лекарственные препараты',
            'Лечение зависимостей',
            'Лечение ЛОР-заболеваний',
            'Мануальная терапия',
            'Медицинские лаборатории',
            'Медицинские приборы / аппараты',
            'Медицинские расходные материалы',
            'Медицинское оборудование / инструмент',
            'Многопрофильные медицинские центры',
            'Морги',
            'Ногтевые студии',
            'Оптика',
            'Ортопедия и травматология',
            'Очищение организма',
            'Парикмахерские',
            'Патронажные услуги',
            'Пластическая хирургия',
            'Поликлиники взрослые',
            'Поликлиники детские',
            'Протезные / ортопедические изделия',
            'Психиатрические учреждения',
            'Реабилитационные центры',
            'Ремонт медицинского оборудования / инструментов',
            'Родильные дома',
            'Санатории / Профилактории',
            'Санитарно-эпидемиологический надзор',
            'Средства гигиены',
            'Станции переливания крови',
            'Стоматологические поликлиники',
            'Стоматологические центры',
            'Стоматологическое оборудование / материалы',
            'Студии загара',
            'Травмпункты',
            'Услуги визажиста',
            'Услуги врача-гомеопата',
            'Услуги гастроэнтеролога',
            'Услуги гинеколога',
            'Услуги дерматовенеролога',
            'Услуги детских специалистов',
            'Услуги имидж-консультанта',
            'Услуги кардиолога',
            'Услуги логопеда',
            'Услуги маммолога',
            'Услуги массажиста',
            'Услуги невролога',
            'Услуги онколога',
            'Услуги проктолога',
            'Услуги психолога',
            'Услуги психотерапевта',
            'Услуги пульмонолога',
            'Услуги сексолога',
            'Услуги уролога / андролога',
            'Услуги флеболога',
            'Услуги эндокринолога',
            'Фельдшерско-акушерские пункты',
            'Фитоцентры',
            'Хоспис',
            'Центры борьбы со СПИДом',
            'Центры планирования семьи',
            'Школы для будущих мам',
            'Красящие вещества',
            'Металлоизделия',
            'Металлообработка',
            'Нержавеющий металлопрокат',
            'Нефтепродукты / ГСМ / Газ',
            'Производство изделий из пластмасс',
            'Промышленная химия / Химическое сырьё',
            'Сварочные материалы',
            'Технические газы / Криогенные жидкости',
            'Уголь',
            'Утилизация отходов / Вторсырьё',
            'Ферросплавы',
            'Цветной металлопрокат',
            'Чёрный металлопрокат',
            'Абразивный инструмент',
            'Банковское оборудование',
            'Бензиновое / дизельное оборудование',
            'Бензоинструмент',
            'Буровое оборудование',
            'Весовое оборудование',
            'Газовое оборудование',
            'Гидравлическое оборудование / инструмент',
            'Горно-шахтное оборудование',
            'Деревообрабатывающее оборудование',
            'Деревообрабатывающий инструмент',
            'Заточка / шлифовка режущих инструментов',
            'Звуковое / световое / видеооборудование',
            'Измерительный инструмент',
            'Каркасно-тентовые конструкции',
            'ККМ / Расходные материалы',
            'Клининговое оборудование / инвентарь',
            'Кондиционеры',
            'Котельное оборудование / Котлы',
            'Малярный инструмент',
            'Металлообрабатывающее оборудование',
            'Металлорежущий инструмент',
            'Насосное оборудование',
            'Нефтегазовое оборудование',
            'Оборудование для автоматизации промышленных предприятий',
            'Оборудование для автоматизации торговли',
            'Оборудование для автосервиса',
            'Оборудование для АЗС и нефтебаз',
            'Оборудование для лабораторий',
            'Оборудование для нанесения полимерных покрытий',
            'Оборудование для пищевого производства',
            'Оборудование для предприятий общественного питания',
            'Оборудование для салонов красоты',
            'Оборудование для сельского хозяйства',
            'Оборудование для энергосбережения',
            'Очистители воздуха',
            'Пневматическое / компрессорное оборудование',
            'Пневмоинструмент',
            'Подшипники',
            'Полиграфическое оборудование',
            'Пресс-формы / Штампы',
            'Продажа / установка тёплых полов',
            'Продажа инфракрасных кабин',
            'Продажа платёжных терминалов / информационных киосков',
            'Проекционное оборудование',
            'Прокат мультимедийного / презентационного оборудования',
            'Прокат оборудования / инструментов',
            'Резинотехнические изделия',
            'Ремонт климатического оборудования',
            'Ремонт промышленного оборудования',
            'Ремонт электроинструмента',
            'Сварочное оборудование',
            'Слесарно-монтажный инструмент',
            'Стеллажи / Витрины',
            'Стропы / Грузозахватывающее оборудование',
            'Тепловентиляционное оборудование',
            'Техника для склада',
            'Торгово-выставочное оборудование',
            'Холодильное оборудование',
            'Швейное оборудование',
            'Электроинструмент',
            'Автошколы',
            'Академии',
            'Бизнес-тренинги / семинары',
            'Бухгалтерские курсы',
            'Гимназии',
            'Детские музыкальные школы',
            'Детские сады / Ясли',
            'Дизайнерские курсы',
            'Институты',
            'Кадровые / рекрутинговые агентства',
            'Колледжи',
            'Компьютерные курсы',
            'Конструкторские бюро',
            'Лицеи',
            'Личностные тренинги / семинары',
            'Межшкольные учебные комбинаты',
            'Научно-исследовательские институты',
            'Начальные школы-детские сады / Прогимназии',
            'Обучение бизнес-профессиям',
            'Обучение за рубежом',
            'Обучение мастеров для салонов красоты',
            'Обучение по охране труда',
            'Обучение рабочим профессиям',
            'Обучение сотрудников охраны',
            'Обучение творчеству и рукоделию',
            'Перевод с иностранных языков',
            'Помощь в обучении',
            'Профессиональная переподготовка / Повышение квалификации',
            'Профессиональные лицеи',
            'Техникумы',
            'Университеты',
            'Училища',
            'Художественные школы',
            'Центры раннего развития / дошкольного образования детей',
            'Школы',
            'Школы иностранных языков',
            'Школы искусств',
            'Школы-интернаты',
            'Бижутерия',
            'Верхняя одежда',
            'Головные уборы',
            'Детская одежда / обувь',
            'Джинсовая одежда',
            'Женская одежда',
            'Игрушки',
            'Магазины обувные',
            'Мех / Кожа - сырьё',
            'Меха / Дублёнки / Кожа',
            'Мужская одежда',
            'Нижнее бельё',
            'Обувная косметика / Аксессуары',
            'Обувь - опт',
            'Одежда для беременных',
            'Ремонт обуви / кожгалантереи',
            'Свадебные салоны',
            'Секонд-хенд',
            'Спецобувь',
            'Спецодежда / Средства индивидуальной защиты',
            'Сумки / Кожгалантерея',
            'Сценические / карнавальные костюмы',
            'Товары для новорождённых',
            'Трикотажные изделия',
            'Чулочно-носочные изделия',
            'Вневедомственная охрана',
            'Детективные услуги',
            'Детекторы лжи',
            'Монтаж охранно-пожарных систем',
            'Огнезащитная обработка',
            'Продажа / установка домофонов',
            'Противопожарное оборудование / инвентарь',
            'Системы сигнализации и охраны',
            'Спасательное оборудование',
            'Услуги охраны',
            'Алкогольные напитки',
            'Безалкогольные напитки',
            'Детское питание',
            'Диетические / Соевые продукты',
            'Жир / Маслопродукты',
            'Зерно / Зерноотходы',
            'Киоски / магазины фастфудной продукции',
            'Колбасные изделия',
            'Кондитерские изделия',
            'Консервированная продукция',
            'Макаронные изделия',
            'Молочные продукты',
            'Мороженое',
            'Мука / Крупы',
            'Мясо / Полуфабрикаты',
            'Мясо птицы / Полуфабрикаты',
            'Овощи / Фрукты / Ягоды / Грибы',
            'Пиво',
            'Продажа / доставка питьевой воды',
            'Продовольственные магазины',
            'Продукты быстрого приготовления',
            'Продукты пчеловодства',
            'Рыба / Морепродукты',
            'Сахар / Соль',
            'Снэковая продукция',
            'Специи / Пряности',
            'Сыры',
            'Сырьё для пищевой промышленности',
            'Табачные изделия / Товары для курения',
            'Хлебобулочные изделия',
            'Чай / Кофе',
            'Яйцо',
            'Indoor-реклама (реклама в помещениях)',
            'PR / Связи с общественностью',
            'Агентства по подписке печатных изданий',
            'Бумага для полиграфии',
            'Видеостудии',
            'Газеты',
            'Дизайн рекламы',
            'Директ-мэйл',
            'Журналы',
            'Изготовление бизнес-сувениров',
            'Изготовление рекламных конструкций',
            'Киоски по продаже печатной продукции',
            'Контент-продукты ГИС',
            'Маркетинговые / социологические исследования',
            'Материалы для наружной рекламы',
            'Модельные агентства',
            'Оперативная полиграфия',
            'Организация и проведение промоушн-акций',
            'Офсетная печать',
            'Перетяжки от &quot;Рекламное Агентство Европа&quot;',
            'Пиллары от &quot;Рекламное Агентство Европа&quot;',
            'Полиграфические услуги',
            'Послепечатная обработка',
            'Предпечатная подготовка',
            'Призматроны от &quot;Рекламное Агентство Европа&quot;',
            'Призматроны от РА &quot;Proline-City&quot;',
            'Производство пластиковых карт',
            'Радиостанции',
            'Размещение наружной рекламы',
            'Размещение рекламы в интернете',
            'Размещение рекламы в СМИ',
            'Размещение рекламы на транспорте',
            'Расходные материалы для полиграфии',
            'Рекламные агентства полного цикла',
            'Рекламные щиты от &quot;Рекламное Агентство Европа&quot;',
            'Рекламные щиты от РА &quot;Proline-City&quot;',
            'Световая реклама',
            'Светодиодные экраны от РА &quot;Proline-City&quot;',
            'Согласование наружной рекламы',
            'Справочники',
            'Студии звукозаписи',
            'Тампопечать',
            'Теле- / радиокомпании',
            'Телеканалы',
            'Термопечать',
            'Тиражирование дисков',
            'Услуги гравировки',
            'Флексопечать',
            'Фотостудии',
            'Шелкография',
            'Широкоформатная печать / УФ-печать',
            'Авиационные клубы',
            'Базы отдыха',
            'Бассейны',
            'Велосипеды',
            'Гостиницы',
            'Детские лагеря',
            'Детский отдых в Горной Шории',
            'Детское игровое оборудование / Аксессуары',
            'Квартирные бюро',
            'Конные клубы / Ипподромы',
            'Ледовые дворцы / Катки',
            'Лодки / Катера',
            'Лыжные базы / Горнолыжные комплексы',
            'Магазины в Шерегеше',
            'Отдых и развлечения в Шерегеше',
            'Охотничьи принадлежности / Оружие',
            'Проживание в Шерегеше',
            'Прокат спортивного инвентаря',
            'Прокат спортивного инвентаря и снаряжения в Шерегеше',
            'Профессиональные спортивные клубы',
            'Ремонт велосипедов',
            'Рыболовные принадлежности',
            'Снаряжение для туризма и отдыха',
            'Спортивная одежда / обувь',
            'Спортивное оборудование',
            'Спортивное питание',
            'Спортивно-интеллектуальные клубы',
            'Спортивно-развлекательное оборудование / Аксессуары',
            'Спортивно-технические клубы',
            'Спортивные секции',
            'Спортивные школы',
            'Спортивный инвентарь',
            'Стадионы',
            'Танцевальные школы',
            'Теннисные клубы / корты',
            'Тренажёрные залы',
            'Туристические агентства',
            'Федерации спорта',
            'Фитнес-клубы',
            'Центры йоги',
            'Автоматические ворота / двери',
            'Бетон / Раствор',
            'Входные двери',
            'Герметики / Клеи',
            'Гидроизоляционные материалы',
            'Гипсокартон / Комплектующие',
            'Декоративные элементы / покрытия',
            'ДСП / ДВП / Фанера',
            'Железобетонные изделия',
            'Заборы / Ограждения',
            'Замки / Скобяные изделия',
            'Звукоизоляционные материалы',
            'Изготовление художественных витражей / мозаики',
            'Керамическая плитка / Кафель',
            'Керамогранит',
            'Кирпич / Бетоноблоки / Шлакоблоки',
            'Комплектующие для дверей',
            'Комплектующие для окон',
            'Крепёжные изделия',
            'Кровельные материалы',
            'Лакокрасочные материалы',
            'Магазины отделочных материалов',
            'Магазины строительных материалов',
            'Материалы для дорожного строительства',
            'Межкомнатные двери',
            'Металлоконструкции для строительства зданий / сооружений',
            'Напольные покрытия / Комплектующие',
            'Облицовочный камень',
            'Обои',
            'Оборудование для производства строительных материалов',
            'Огнезащитные материалы',
            'Огнеупорные материалы / изделия',
            'Окна',
            'Оргстекло / Поликарбонат',
            'Песок / Щебень',
            'Пиломатериалы',
            'Погонажные изделия',
            'Порошковые краски',
            'Продажа / монтаж потолков',
            'Системы перегородок',
            'Стекло / Зеркала',
            'Стеновые блоки',
            'Стеновые панели',
            'Сухие строительные смеси',
            'Сэндвич-панели',
            'Теплоизоляционные материалы',
            'Тротуарная плитка',
            'Фасадные материалы / конструкции',
            'Автоматизация инженерных систем',
            'Агентства недвижимости',
            'Алмазное бурение / резка',
            'Антикоррозийная обработка металлоконструкций',
            'Аренда банкетных / конференц-залов',
            'Аренда помещений',
            'Архитектурно-строительное проектирование',
            'Бизнес-центры',
            'Буровые работы',
            'Быстровозводимые здания / сооружения',
            'Бюро технической инвентаризации (БТИ)',
            'Взрывные работы',
            'Высотные работы',
            'Геодезические работы',
            'Геологические работы',
            'Геофизические работы',
            'Гидромассажное оборудование',
            'Деревообработка',
            'Дизайн интерьеров',
            'Жилищное строительство',
            'Земельно-кадастровые работы',
            'Инжиниринговые услуги',
            'Кровельные работы',
            'Ландшафтная архитектура',
            'Монтаж климатических систем',
            'Монтаж систем отопления / водоснабжения / канализации',
            'Новостройки',
            'Оборудование для очистки воды',
            'Остекление балконов / лоджий',
            'Полимерная порошковая окраска',
            'Продажа / аренда недвижимости за рубежом',
            'Продажа земельных участков / малоэтажных домов',
            'Проектирование инженерных систем',
            'Промышленное строительство',
            'Реконструкция и капремонт зданий',
            'Ремонт / отделка помещений',
            'Ремонт окон',
            'Реставрация ванн',
            'Сантехника / Санфаянс',
            'Свайные работы',
            'Сварочные работы',
            'Системы отопления / водоснабжения / канализации',
            'Согласование перепланировок',
            'Строительная техника',
            'Строительное оборудование',
            'Строительные леса / Вышки-туры',
            'Строительство / обслуживание электросетей',
            'Строительство / ремонт дорог',
            'Строительство / ремонт наружных инженерных сетей',
            'Строительство административных зданий',
            'Строительство АЗС / АГЗС',
            'Строительство бань / саун',
            'Строительство гаражей',
            'Строительство дач / коттеджей',
            'Строительство и монтаж бассейнов / фонтанов',
            'Строительство мостов / тоннелей / путепроводов',
            'Строительство систем газоснабжения',
            'Теплоизоляционные работы',
            'Техническая экспертиза зданий и сооружений',
            'Тонирование стёкол зданий / конструкций',
            'Услуги по устройству промышленных / наливных полов',
            'Фасадные работы',
            'Электроизмерительные работы',
            'Электромонтажные работы',
            'Жалюзи / Рольставни',
            'Интерьерные лестницы / Ограждения',
            'Карнизы',
            'Керамические изделия',
            'Кованые изделия',
            'Ковры',
            'Нетканые материалы',
            'Печи / Камины',
            'Портьерные ткани / Шторы',
            'Постельные принадлежности / Текстиль для дома',
            'Постельные принадлежности / Текстиль для дома',
            'Пряжа',
            'Ткани',
            'Швейная фурнитура',
            'Аквариумы',
            'Ветеринарные аптеки',
            'Ветеринарные клиники',
            'Зоотовары',
            'Клубы домашних животных / Питомники',
            'Комбикорм / Кормовые добавки',
            'Приюты для животных',
            'Аксессуары для бань и саун',
            'Гипермаркеты',
            'Доставка цветов',
            'Интернет-магазины',
            'Комиссионные магазины',
            'Новогодние товары',
            'Ремонт / изготовление ювелирных изделий',
            'Рынки',
            'Сборные / радиоуправляемые модели',
            'Сувениры / Подарки',
            'Супермаркеты',
            'Товары для творчества и рукоделия',
            'Торгово-развлекательные центры / Моллы',
            'Торговые центры',
            'Универсальные магазины / комплексы',
            'Цветы',
            'Часы',
            'Эротические товары',
            'Ювелирные изделия',
            'Авиабилеты',
            'Авиагрузоперевозки',
            'Авиакомпании',
            'Автобусные билеты',
            'Автовокзал',
            'Автоэкспертиза',
            'Аэропорты',
            'Вывоз мусора / снега',
            'Гаражные кооперативы',
            'Городские автогрузоперевозки',
            'Железнодорожное оборудование / техника',
            'Железнодорожные билеты',
            'Железнодорожные вокзалы и станции',
            'Железнодорожные грузоперевозки',
            'Заказ автобусов',
            'Заказ пассажирского легкового транспорта',
            'Заказ строительной / спецавтотехники',
            'Материалы для железнодорожных путей',
            'Междугородные автогрузоперевозки',
            'Международные грузоперевозки',
            'Морские / речные грузоперевозки',
            'Мототехника',
            'Оформление купли-продажи автомобилей',
            'Пассажирские автотранспортные предприятия (ПАТП)',
            'Продажа автобусов',
            'Продажа грузовых автомобилей',
            'Продажа легковых автомобилей',
            'Прокат автотранспорта',
            'Речной вокзал',
            'Сельхозтехника',
            'Спецавтотехника',
            'Строительство / ремонт железных дорог',
            'Трамвайные депо',
            'Троллейбусные депо',
            'Услуги грузчиков',
            'Услуги складского хранения',
            'Экспедирование грузов',
            'Экспресс-почта',
            'Бумажная упаковка',
            'Бытовая химия',
            'Гофротара',
            'Изготовление / продажа теплиц',
            'Календари / Открытки - опт',
            'Канцелярские товары',
            'Книги',
            'Оборудование для промышленной маркировки',
            'Одноразовая посуда',
            'Офисная бумага',
            'Пакеты / Плёнки',
            'Пластиковая тара',
            'Подарочная упаковка',
            'Посуда',
            'Садово-огородный инвентарь / техника',
            'Семена / Посадочный материал',
            'Средства защиты растений / Удобрения',
            'Укрывной материал',
            'Упаковочные материалы',
            'Учебная литература',
            'Фасовочно-упаковочное оборудование',
            'Хозяйственные товары',
            'Геодезическое оборудование',
            'Кабель / Провод',
            'Контрольно-измерительные приборы (КИПиА)',
            'Оптические приборы',
            'Радиоэлектронные приборы',
            'Ремонт электродвигателей',
            'Светотехника',
            'Электродвигатели / Редукторы',
            'Электроизоляционные материалы',
            'Электронагревательное оборудование',
            'Электронные компоненты',
            'Электротехническая продукция',
            'Электроустановочная продукция',
            'Элементы питания',
            'Автокредитование',
            'Арбитражный управляющий',
            'Аттестация рабочих мест',
            'Аудиторские услуги',
            'Банки',
            'Банкоматы',
            'Бизнес-инкубаторы',
            'Бухгалтерские услуги',
            'Ведение дел в судах',
            'Дилинговые центры',
            'Защита авторских прав',
            'Изготовление печатей / штампов',
            'Инвестиционные компании',
            'Ипотека / Жилищное кредитование',
            'Кассы обмена валют',
            'Коллекторские услуги',
            'Кредитные потребительские кооперативы граждан',
            'Лизинговые услуги',
            'Лицензирование / Сертификация',
            'Ломбарды',
            'Миграционные услуги',
            'Микрофинансирование',
            'Негосударственные пенсионные фонды',
            'Нотариальные услуги',
            'Операции на фондовом рынке',
            'Организация выставок',
            'Оформление недвижимости / земли',
            'Оценка собственности',
            'Паевые инвестиционные фонды (ПИФы)',
            'Патентные услуги',
            'Пожарная безопасность',
            'Помощь в оформлении допуска СРО',
            'Помощь в оформлении ипотеки',
            'Регистрация / ликвидация предприятий',
            'Регистрация ценных бумаг',
            'Саморегулируемые организации (СРО)',
            'Страхование',
            'Таможенное оформление',
            'Управленческий консалтинг',
            'Услуги факторинга',
            'Финансовый консалтинг',
            'Экологическая оценка',
            'Экспертиза промышленной безопасности',
            'Экспертиза товаров народного потребления',
            'Энергоаудит',
            'Юридическое обслуживание',
        ];
        $days = ['Mon' => 'Пн', 'Tue' => 'Вт', 'Wed' => 'Ср', 'Thu' => 'Чт', 'Fri' => 'Пт', 'Sat' => 'Сб', 'Sun' => 'Вс'];
        /** @var PDO $db */
        $db = DB::getInstance();
        // берем только города
        $stmn = $db->query('SELECT id, name FROM settlement WHERE region_id = ' . $region . ' AND type_id = 1');
        $arr = [];
        while ($row = $stmn->fetch(PDO::FETCH_ASSOC)) {
            $arr[$row['id']] = $row['name'];
        }
        // смотрим, что есть в 2гис
        $arr = $this->_2gisProject($arr);
        if (empty($arr)) {
            $this->_message('In 2gis not exist settlements!');
        }
        $f = function($url, $settId) use ($days) {
            $sx = true;
            $p = 1;
            /** @var PDO $db */
            $db = DB::reConnect();
            do {
                $res = $this->_send($url . $p);
                if (empty($res)) {
                    // какая-то беда
                    $this->_log('Response from 2gis empty!');
                    break;
                }
                $res = json_decode($res, true);
                if ($res['response_code'] == 200) {
                    foreach($res['result'] as $v) {
                        if (empty($v['address'])) continue;
                        $a = explode(',', $v['address']);
                        $a[0] = trim($a[0]);
                        $stmn = $db->query('SELECT id FROM street WHERE `name` = ' . $db->quote($a[0]) . ' AND settlement_id = ' . $settId . ' LIMIT 1');
                        if ($stmn->rowCount() > 0) {
                            // есть такая улица
                            $a[1] = trim($a[1]);
                            $b = explode(' ', $a[1]);
                            $streetId = $stmn->fetchColumn();
                            $stmn = $db->query('SELECT id FROM house WHERE street_id = ' . $streetId . ' AND house = ' . $db->quote($b[0]));
                            if ($stmn->rowCount() > 0) {
                                $houseId = $stmn->fetchColumn();
                                $stmn = $db->query('SELECT id FROM house_organizations WHERE `name` = ' . $db->quote($v['name']) . ' AND house_id = ' . $houseId);
                                if ($stmn->rowCount() > 0) {
                                    // организация уже существует пропускаем
                                    $this->_log("Organization '" . $v['name'] . "' from '" . $v['address'] . "' exists id = " . $stmn->fetchColumn());
                                } else {
                                    // запрос за подробностями
                                    $this->_log("Request 2gis profile firm '" . $v['name'] . "' from '" . $v['address'] . "'");
                                    $r = $this->_send('http://catalog.api.2gis.ru/profile?&version=1.3&key=' . $this->api2gisKey . '&id=' . $v['id'] . '&hash=' . $v['hash']);
                                    if (empty($r)) {
                                        // какая-то беда
                                        $this->_log('Response 2gis profile firm empty!');
                                    }
                                    $r = json_decode($r, true);
                                    $grafik = '';
                                    foreach ($days as $k => $d) {
                                        if (!empty($r['schedule'][$k])) {
                                            $grafik .= "$d<br/>\n" . (!empty($r['schedule'][$k]['working_hours-0']) ? $r['schedule'][$k]['working_hours-0']['from'] . ' - ' . $r['schedule'][$k]['working_hours-0']['to'] . "<br/>\n" : '')
                                                . (!empty($r['schedule'][$k]['working_hours-1']) ? $r['schedule'][$k]['working_hours-1']['from'] . ' - ' . $r['schedule'][$k]['working_hours-1']['to'] . "<br/>\n" : '');
                                        }
                                    }
                                    $grafik .= (!empty($r['schedule']['comment']) ? $r['schedule']['comment'] : '');
                                    $phones = $sites = $emails = '';
                                    if (!empty($r['contacts'])) {
                                        foreach($r['contacts'] as $coo) {
                                            foreach($coo['contacts'] as $c) {
                                                if ($c['type'] == 'phone') {
                                                    $phones .= $c['value'] . "<br/>\n";
                                                } elseif($c['type'] == 'website') {
                                                    $sites .= (!empty($c['alias']) ? $c['alias'] : $c['value']) . "<br/>\n";
                                                } elseif($c['type'] == 'email') {
                                                    $emails .= $c['value'] . "<br/>\n";
                                                }
                                            }
                                        }
                                    }
                                    $stmn = $db->prepare('INSERT INTO house_organizations (`name`, `grafik`, `phones`, `sites`, `emails`, `house_id`) VALUE (?,?,?,?,?,?)');
                                    try {
                                        $stmn->execute([$v['name'], $grafik, $phones, $sites, $emails, $houseId]);
                                        $this->_log("Add organization '" . $v['name'] . "' from '" . $v['address'] . "' exists id = " . $db->lastInsertId());
                                    } catch (PDOException $e) {
                                        $this->_log('Already exist organiz.');
                                    }
                                }
                            } else {
                                $this->_log('Could not find the address of the organization!');
                            }
                        }
                    }
                } else {
                    $sx = false;
                }
                // @todo: разбираем и записываем
                ++$p;
            } while ($sx);
        };
        foreach($arr as $settId => $setName) {
            foreach($rubrics as $v) {
                $url = 'http://catalog.api.2gis.ru/searchinrubric?what=' . urlencode($v) . '&where=' . urlencode($setName) .'&pagesize=50&sort=name&version=1.3&key=' . $this->api2gisKey . '&page=';
                if ((count($this->_workerPids) < $this->workerCount) && $this->_runMainLoop) {
                    $workerPid = pcntl_fork();
                    if ($workerPid) {
                        $this->_workerPids[$workerPid] = 1;
                    } else {
                        $this->_parentProc = false;
                        $f($url, $settId);
                        exit(0);
                    }
                } else {
                    $sPid = pcntl_waitpid(-1, $status, WUNTRACED);
                    unset($this->_workerPids[$sPid]);
                    if ($this->_runMainLoop) {
                        $workerPid = pcntl_fork();
                        if ($workerPid) {
                            $this->_workerPids[$workerPid] = 1;
                        } else {
                            $this->_parentProc = false;
                            $f($url, $settId);
                            exit(0);
                        }
                    }
                }
            }
        }
        $this->_waitChild();
        $this->_message('2gis successful!');
    }
    public function reset()
    {
        /** @var PDO $db */
        $db = DB::getInstance();
        $db->exec('set foreign_key_checks=0;
            drop table settlement;
            drop table street;
            drop table house;
            drop table house_organizations;
        ');
        $dir = realpath($this->basePath . '/../attach_data/');
        exec('mysql -u root -p1234 tsg_test < ' . $dir . '/settlement.sql &');
        exec('mysql -u root -p1234 tsg_test < ' . $dir . '/street.sql &');
        exec('mysql -u root -p1234 tsg_test < ' . $dir . '/house.sql &');
        exec('mysql -u root -p1234 tsg_test < ' . $dir . '/house_organizations.sql &');
        echo "ok!\n";
    }
    public function del()
    {
        /** @var PDO $db */
        echo "Del\n";
        $workerPid = pcntl_fork();
        if ($workerPid) {
            $db = DB::reConnect();
            $f1 = $this->basePath . '/uploads/pre/AS_DEL_ADDROBJ_20150205_91d80a2b-f52b-47e5-b259-f0a92c5cc201_aa';
            $fp = fopen($f1, 'r');
            $arr = [];
            $i = $c = 0;
            while(($line = fgets($fp, 4096)) !== false) {
                $patern = '#AOID="(.*?)"#';
                if (preg_match($patern, $line, $match)) {
                    $arr[] = $match[1];
                }
                if ($i == 99) {
                    $db->exec('DELETE FROM a_object WHERE aoid IN("'.implode('","', $arr).'")');
                    $c += $i;
                    $arr = [];
                    $i = 0;
                }
                ++$i;
            }
            if ($i) {
                $db->exec('DELETE FROM a_object WHERE aoid IN("'.implode('","', $arr).'")');
                $c += $i;
            }
            echo "Delete ($c) object!\n";
            exit(0);
        } else {
            $db = DB::reConnect();
            $f1 = $this->basePath . '/uploads/pre/AS_DEL_HOUSE_20150205_761f5f27-0016-4093-a33e-11fa046f0120_aa';
            $fp = fopen($f1, 'r');
            $arr = [];
            $i = $c = 0;
            while(($line = fgets($fp, 4096)) !== false) {
                $patern = '#HOUSEID="(.*?)"#';
                if (preg_match($patern, $line, $match)) {
                    $arr[] = $match[1];
                }
                if ($i == 99) {
                    $db->exec('DELETE FROM a_house WHERE houseid IN("'.implode('","', $arr).'")');
                    $c += $i;
                    $arr = [];
                    $i = 0;
                }
                ++$i;
            }
            if ($i) {
                $db->exec('DELETE FROM a_house WHERE houseid IN("'.implode('","', $arr).'")');
                $c += $i;
            }
            echo "Delete ($c) house!\n";
            exit(0);
        }
    }

    /**
     * @param int $region
     */
    private function _migration3($region)
    {
        /** @var PDO $db */
        $db = DB::reConnect();
        $db->exec('UPDATE settlement SET available = 1 WHERE region_id = ' . $region . ' AND type_id = 1');
    }
}

$app = new App([
    'basePath' => '/var/webserver/tsg66/www',
    'pidPath' => '/var/webserver/tsg66/www/pids/tsg66.pid',
    'db' => [
        'connectionString' => 'mysql:host=localhost;port=3306;dbname=tsg_test',
        'username' => 'root',
        'password' => '1234',
        'charset' => 'utf8',
    ],
    'workerCount' => 10,
    'api2gisKey' => 'ruvqwa9223',
]);
$app->run();
?>